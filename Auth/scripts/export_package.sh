#!/bin/zsh
#
# Export the project to "*.unitypackage".
# Warning! MacOS Edition!
#
# How to use.
# 1. Open terminal and go to this script directory (cd path_to_this_script_dir").
# 2. Run script with a preferable package "version name" as argument:
#    Commands example:
#        a) export_package.sh 1.0.0 <path_to_unity>
#        b) bash export_package.sh 1.2.3 <path_to_unity>
# 3. If successful, the exported package will be in the project's root.

cd "$(dirname "${BASH_SOURCE[0]}")/.." || exit
PROJECT_ROOT=$(pwd)

declare -r OUTPUT_DIR="out"
declare -r OUTPUT_PROJECT_DIR="project"
declare -r ERO_PACKAGE_NAME="ErogamesAuth"
declare -r ERO_EXPORT_PACKAGE="Assets"
declare -r ERO_EXPORT_PACKAGE_VERSION="${1}"
declare -r ERO_EXPORT_PACKAGE_NAME="erogamesauth-${ERO_EXPORT_PACKAGE_VERSION}.unitypackage"
declare -r ERO_TEMP_PROJECT_PATH="${PROJECT_ROOT}/${OUTPUT_DIR}/${OUTPUT_PROJECT_DIR}"

declare -r UNITY_PATH="${2}"
declare -r UNITY_JAR_RESOLVER_NAME='unity-jar-resolver'
declare -r UNITY_JAR_RESOLVER_VERSION='1.2.165'
declare -r UNITY_JAR_RESOLVER_PACKAGE_NAME='external-dependency-manager'
declare -r UNITY_JAR_RESOLVER_BASE_URL="https://github.com/googlesamples/${UNITY_JAR_RESOLVER_NAME}/archive/v"
declare -r UNITY_JAR_RESOLVER_ZIP_URL="${UNITY_JAR_RESOLVER_BASE_URL}${UNITY_JAR_RESOLVER_VERSION}.zip"
declare -r UNITY_JAR_RESOLVER_PACKAGE=${UNITY_JAR_RESOLVER_PACKAGE_NAME}-${UNITY_JAR_RESOLVER_VERSION}.unitypackage
declare -r UNITY_PACKAGE_PATH=${PROJECT_ROOT}/${OUTPUT_DIR}/${UNITY_JAR_RESOLVER_NAME}/${UNITY_JAR_RESOLVER_PACKAGE}

COLOR_RED=$(tput setaf 1)
COLOR_GREEN=$(tput setaf 2)
STYLE_RESET=$(tput sgr0)
TEXT_BOLD=$(tput bold)
readonly COLOR_RED
readonly COLOR_GREEN
readonly STYLE_RESET
readonly TEXT_BOLD

#######################################
# Сolored 'echo'
# Example 1:
#   echolog "some_string"
#   out: {prefix} some_string
# Example 2:
#   echolog "some_string" ""
#   out: some_string
# Example 3:
#   echolog "some_string" "$COLOR_RED"
#   out: {RED prefix} some_string
# Globals:
#   COLOR_GREEN, STYLE_RESET
# Arguments:
#    String to output.
#    (Option) Prefix color. Prefix will not be printed if this arg is empty.
#######################################
function echolog() {
  if [[ "$#" -gt 1 ]]; then
    if [ -z "${2}" ]; then
      prefix=""
      prefix_color="$STYLE_RESET"
    else
      prefix="==>"
      prefix_color="${2}"
    fi
  else
    prefix="==>"
    prefix_color="$COLOR_GREEN"
  fi
  echo "${prefix_color}${prefix}${STYLE_RESET} ${1}"
}

function die() {
  echolog "ERROR: $*" "$COLOR_RED" >&2
  exit 1
}

exportPackageWithUnityJarResolver() {
  if [[ -z "$ERO_EXPORT_PACKAGE_VERSION" || -z "$UNITY_PATH" ]]; then
      echolog "Missed arguments" "$COLOR_RED"
      exit 1
  fi

  pushd "$PROJECT_ROOT" >/dev/null || die

  echolog "Make output directory: $OUTPUT_DIR"
  mkdir -p "$OUTPUT_DIR"
  cd "$OUTPUT_DIR" || exit

  echolog "Downloading unity-jar-resolver..."
  curl -L "$UNITY_JAR_RESOLVER_ZIP_URL" >$UNITY_JAR_RESOLVER_NAME.zip || die "Failed to download $UNITY_JAR_RESOLVER_URL"
  echolog "Unzip..."
  unzip -o -j -q $UNITY_JAR_RESOLVER_NAME.zip -d $UNITY_JAR_RESOLVER_NAME

  echolog "Make ${OUTPUT_PROJECT_DIR}/Assets dir..."
  mkdir -p "${OUTPUT_PROJECT_DIR}/Assets"

  echolog "Copy package to \"${ERO_TEMP_PROJECT_PATH}/Assets\" directory..."
  cp -R "${PROJECT_ROOT}/Assets/${ERO_PACKAGE_NAME}" "${ERO_TEMP_PROJECT_PATH}/Assets"

  echolog "Temp project path: ${TEXT_BOLD}${ERO_TEMP_PROJECT_PATH}${STYLE_RESET}"
  echolog "Import package: ${TEXT_BOLD}${UNITY_PACKAGE_PATH}${STYLE_RESET}"
  echolog "Export package: ${TEXT_BOLD}${ERO_EXPORT_PACKAGE}${STYLE_RESET}"
  echolog "Export package name: ${TEXT_BOLD}${ERO_EXPORT_PACKAGE_NAME}${STYLE_RESET}"
  echolog "Exporting..."

  $UNITY_PATH -gvh_disable \
    -batchmode \
    -importPackage "${UNITY_PACKAGE_PATH}" \
    -projectPath "${ERO_TEMP_PROJECT_PATH}" \
    -exportPackage "${ERO_EXPORT_PACKAGE}" "${ERO_EXPORT_PACKAGE_NAME}" \
    -quit

  cp "${ERO_TEMP_PROJECT_PATH}/${ERO_EXPORT_PACKAGE_NAME}" "${PROJECT_ROOT}"

  echolog "Cleanup..."
  cd "${PROJECT_ROOT}" || exit
  rm -rf $OUTPUT_DIR
  popd >/dev/null || die
}

exportPackageWithUnityJarResolver
