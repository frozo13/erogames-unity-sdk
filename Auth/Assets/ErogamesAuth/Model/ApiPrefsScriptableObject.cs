﻿using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class ApiPrefsScriptableObject : ScriptableObject
    {
        static readonly string AssetName = "ApiPrefsScriptableObject";
        static readonly string AssetDirectoryPath = "Assets/ErogamesAuth/Data/Resources/";
        static readonly string AssetFullPath = AssetDirectoryPath + AssetName + ".asset";

        public string whitelabelId;
        public string clientId;
        public bool autoLogin = true;
        public string accessKey;

        internal static ApiPrefsScriptableObject Load()
        {
            return Resources.Load<ApiPrefsScriptableObject>(AssetName);
        }

#if UNITY_EDITOR
        public static ApiPrefsScriptableObject LoadOrCreate()
        {
            ApiPrefsScriptableObject apiPrefs = Load();
            if (apiPrefs == null)
            {
                if(!Directory.Exists(AssetDirectoryPath))
                    Directory.CreateDirectory(Path.GetDirectoryName(AssetDirectoryPath));

                apiPrefs = CreateInstance<ApiPrefsScriptableObject>();
                AssetDatabase.CreateAsset(apiPrefs, AssetFullPath);
            }

            Debug.Log("LoadOrCreate", apiPrefs);
            return apiPrefs;
        }

        public static void Save(ApiPrefsScriptableObject apiPrefsScriptableObject)
        {
            Debug.Log("Save", apiPrefsScriptableObject);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif
    }
}
