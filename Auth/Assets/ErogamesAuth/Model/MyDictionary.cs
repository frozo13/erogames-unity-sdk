﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class StringStringDictionary : Dictionary<string, string>, ISerializationCallbackReceiver
    {
        public List<string> _keys = new List<string>();
        public List<string> _values = new List<string>();

        public void OnBeforeSerialize()
        {
            _keys.Clear();
            _values.Clear();
            foreach (KeyValuePair<string, string> entry in this)
            {
                _keys.Add(entry.Key);
                _values.Add(entry.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            for (int i = 0; i != Math.Min(_keys.Count, _values.Count); i++)
                Add(_keys[i], _values[i]);
        }
    }
}
