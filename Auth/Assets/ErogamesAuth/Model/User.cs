﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class User
    {
        public string id;
        public string email;
        public string username;
        public string avatar_url;
        public string language;
        public int balance;
    }
}
