﻿using System;
using System.Collections.Generic;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class DataPointModel
    {
        public List<DataPoint> data_points;

        public DataPointModel(List<DataPoint> data_points)
        {
            this.data_points = data_points;
        }
    }
}

