﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class QuestData : ISerializationCallbackReceiver
    {
        public Quest quest;
        public List<Leader> leaders;
        public List<BestPlayer> best_players;
        public UserClanAttempt user_clan_attempt;
        public UserAttempt user_attempt;
        public UserScore user_score;

        public void OnAfterDeserialize()
        {
            if (user_clan_attempt.IsDummy()) user_clan_attempt = null;
            if (user_attempt.IsDummy()) user_attempt = null;
        }
        public void OnBeforeSerialize() { }

        [Serializable]
        public class Quest
        {
            public int id;
            public string title;
            public string description;
            public string cover_url;
            public string finished_at;
        }


        [Serializable]
        public class Leader
        {
            public int position;
            public string name;
            public string cover_picture_url;
            public string clan_leader_name;
            public long score;
        }

        [Serializable]
        public class BestPlayer
        {
            public int position;
            public string name;
            [FormerlySerializedAs("cover_picture_url")]
            public string coverPictureUrl;
            public long score;
        }

        [Serializable]
        public class UserClanAttempt
        {
            public int position;
            public string name;
            public string cover_picture_url;
            public string clan_leader_name;
            public int score;

            internal bool IsDummy()
            {
                return position == 0
                    && string.IsNullOrEmpty(name)
                    && string.IsNullOrEmpty(cover_picture_url)
                    && string.IsNullOrEmpty(clan_leader_name)
                    && position == 0;
            }
        }

        [Serializable]
        public class UserAttempt
        {
            public int position;
            public string name;
            public string cover_picture_url;
            public int score;

            internal bool IsDummy()
            {
                return position == 0
                    && string.IsNullOrEmpty(name)
                    && string.IsNullOrEmpty(cover_picture_url)
                    && position == 0;
            }
        }

        [Serializable]
        public class UserScore
        {
            public int total;
            public List<EarnedPoint> earned_points;
        }


        [Serializable]
        public class EarnedPoint
        {
            public int points;
            public string title;
        }
    }
}
