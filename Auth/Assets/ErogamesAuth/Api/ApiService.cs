﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAuthNS.Api
{
    internal class ApiService : Singleton<ApiService>
    {
        private const string XSDKVersion = "1.1";
        private const string BaseURL = "https://erogames.com/";
        private const string JwtTokenEndPoint = "api/v1/authenticate";
        internal const string RegisterUserEndPoint = "api/v1/register";
        private const string WhitelabelsEndPoint = "api/v1/whitelabels";

        private ApiPrefs apiPrefs;

        /// <summary>
        /// Returns ApiPrefs.
        /// See also <see cref="ApiPrefsUtil.LoadMergedApiPrefs"/>
        /// </summary>
        /// <returns>ApiPrefs</returns>
        private ApiPrefs GetApiPrefs()
        {
            if (apiPrefs == null) apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            return apiPrefs;
        }

        /// <summary>
        /// Returns JWT token.
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            string url = BaseURL + JwtTokenEndPoint;
            WWWForm form = new WWWForm();
            form.AddField("access_key", accessKey);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(url, form))
            {
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    var token = JsonUtility.FromJson<JwtTokenResp>(unityWebRequest.downloadHandler.text).token;
                    onResult.Invoke(Result<string>.Success(token));
                }
                else
                {
                    onResult.Invoke(Result<string>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Returns whitelabel list.
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetWhitelabels(string jwtToken, Action<Result<List<Whitelabel>>> onResult)
        {
            string url = BaseURL + WhitelabelsEndPoint;

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(url))
            {
                unityWebRequest.SetRequestHeader("Authorization", jwtToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    List<Whitelabel> wls = WhitelabelsResp.ToWhitelabels(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<List<Whitelabel>>.Success(wls));
                }
                else
                {
                    onResult.Invoke(Result<List<Whitelabel>>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Returns token.
        /// </summary>
        /// <param name="tokenUrl"></param>
        /// <param name="clientId"></param>
        /// <param name="code"></param>
        /// <param name="redirectUri"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetToken(string tokenUrl, string clientId, string code, string redirectUri, string codeVerifier, Action<Result<Token>> onResult)
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", clientId);
            form.AddField("code", code);
            form.AddField("grant_type", "authorization_code");
            form.AddField("redirect_uri", redirectUri);
            form.AddField("code_verifier", codeVerifier);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(tokenUrl, form))
            {
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    Token token = JsonUtility.FromJson<Token>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<Token>.Success(token));
                }
                else
                {
                    onResult.Invoke(Result<Token>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Returns token by username/password.
        /// </summary>
        /// <param name="tokenUrl"></param>
        /// <param name="clientId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Token if success, throws an exception otherwise.</returns>
        internal IEnumerator GetTokenByPassword(string tokenUrl, string clientId, string username, string password, Action<Result<Token>> onResult)
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", clientId);
            form.AddField("grant_type", "password");
            form.AddField("login", username);
            form.AddField("password", password);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(tokenUrl, form))
            {
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    Token token = JsonUtility.FromJson<Token>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<Token>.Success(token));
                }
                else
                {
                    onResult.Invoke(Result<Token>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Refresh token (in case it is expired).
        /// </summary>
        /// <param name="tokenUrl"></param>
        /// <param name="clientId"></param>
        /// <param name="refreshToken"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator RefreshToken(string tokenUrl, string clientId, string refreshToken, Action<Result<Token>> onResult)
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", clientId);
            form.AddField("grant_type", "refresh_token");
            form.AddField("refresh_token", refreshToken);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(tokenUrl, form))
            {
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    Token token = JsonUtility.FromJson<Token>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<Token>.Success(token));
                }
                else
                {
                    onResult.Invoke(Result<Token>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Returns current user info.
        /// </summary>
        /// <param name="profileUrl"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        internal IEnumerator GetUser(string profileUrl, string accessToken, Action<Result<User>> onResult)
        {
            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(profileUrl))
            {
                unityWebRequest.SetRequestHeader("Authorization", accessToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    User user = JsonUtility.FromJson<UserResp>(unityWebRequest.downloadHandler.text).user;
                    onResult.Invoke(Result<User>.Success(user));
                }
                else
                {
                    onResult.Invoke(Result<User>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="checkTermsOfUse"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator RegisterUser(string registerUrl, string clientSecret, string username, string password, string email, bool checkTermsOfUse, Action<Result<bool>> onResult)
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", GetApiPrefs().clientId);
            form.AddField("client_secret", clientSecret);
            form.AddField("grant_type", "password");
            form.AddField("username", username);
            form.AddField("password", password);
            form.AddField("passwordConfirmation", password);
            form.AddField("email", email);
            form.AddField("checkTermsOfUse", checkTermsOfUse.ToString());

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(registerUrl, form))
            {
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    onResult.Invoke(Result<bool>.Success(true));
                }
                else
                {
                    onResult.Invoke(Result<bool>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Proceed a new payment.
        /// </summary>
        /// <param name="proceedPaymentUrl"></param>
        /// <param name="accessToken"></param>
        /// <param name="paymentId"></param>
        /// <param name="amount"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator ProceedPayment(string proceedPaymentUrl, string accessToken, string paymentId, int amount, Action<Result<BaseStatusResp>> onResult)
        {
            WWWForm form = new WWWForm();
            form.AddField("paymentId", paymentId);
            form.AddField("amount", amount);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(proceedPaymentUrl, form))
            {
                unityWebRequest.SetRequestHeader("Authorization", accessToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    BaseStatusResp resp = JsonUtility.FromJson<BaseStatusResp>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<BaseStatusResp>.Success(resp));
                }
                else
                {
                    onResult.Invoke(Result<BaseStatusResp>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Create/update user data points.
        /// </summary>
        /// <param name="dataPointsUrl"></param>
        /// <param name="accessToken"></param>
        /// <param name="dataPointModel"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator AddDataPoints(string dataPointsUrl, string accessToken, DataPointModel dataPointModel, Action<Result<BaseStatusResp>> onResult)
        {
            string dataPointModelStr = JsonUtility.ToJson(dataPointModel);
            byte[] data = System.Text.Encoding.UTF8.GetBytes(dataPointModelStr);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Put(dataPointsUrl, data))
            {
                unityWebRequest.SetRequestHeader("Content-Type", "application/json");
                unityWebRequest.SetRequestHeader("Authorization", accessToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    BaseStatusResp resp = JsonUtility.FromJson<BaseStatusResp>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<BaseStatusResp>.Success(resp));
                }
                else
                {
                    onResult.Invoke(Result<BaseStatusResp>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Get Current Quest Data
        /// </summary>
        /// <param name="url"></param>
        /// <param name="accessToken"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetCurrentQuestData(string url, string accessToken, Action<Result<QuestData>> onResult)
        {
            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(url))
            {
                unityWebRequest.SetRequestHeader("Content-Type", "application/json");
                unityWebRequest.SetRequestHeader("Authorization", accessToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    QuestData resp = JsonUtility.FromJson<QuestData>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<QuestData>.Success(resp));
                }
                else
                {
                    onResult.Invoke(Result<QuestData>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }



        internal IEnumerator GetPaymentInfo(string url, string accessToken, Action<Result<PaymentInfo>> onResult)
        {
            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(url))
            {
                unityWebRequest.SetRequestHeader("Content-Type", "application/json");
                unityWebRequest.SetRequestHeader("Authorization", accessToken);
                unityWebRequest.SetRequestHeader("X-SDK-VERSION", XSDKVersion);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    PaymentInfo resp = JsonUtility.FromJson<PaymentInfo>(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<PaymentInfo>.Success(resp));
                }
                else
                {
                    onResult.Invoke(Result<PaymentInfo>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }




        /// <summary>
        /// Extract error message from response.
        /// </summary>
        /// <param name="rawResponse"></param>
        /// <returns></returns>
        private string ExtractErrorMessage(string rawResponse)
        {
            ErrorResp errorResp = JsonUtility.FromJson<ErrorResp>(rawResponse);

            if (errorResp == null) return rawResponse;
            if (!string.IsNullOrEmpty(errorResp.message)) return errorResp.message;
            if (errorResp.errors != null && errorResp.errors.Count > 0) return string.Join(", ", errorResp.errors.ToArray());
            if (!string.IsNullOrEmpty(errorResp.error_description)) return errorResp.error_description;

            return rawResponse;
        }
    }
}
