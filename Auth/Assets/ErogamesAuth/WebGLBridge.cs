using System;
using System.Collections;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.UI;
using ErogamesAuthNS.Util;
using ErogamesAuthNS.Repository;
using UnityEngine;
using System.Runtime.InteropServices;

namespace ErogamesAuthNS
{
    internal class WebGLBridge : Singleton<WebGLBridge>
    {
        private bool isSignup = false;
        private string currentLang = "en";

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void TakeCodeFromIframe(string authUrl);

        [DllImport("__Internal")]
        private static extern string GetWindowNameInternal();

        [DllImport("__Internal")]
        private static extern void ReplaceHistoryStateInternal(string url);

        [DllImport("__Internal")]
        private static extern void WebLogoutInternal(string url);

        [DllImport("__Internal")]
        private static extern string GetItem(string key);

        [DllImport("__Internal")]
        private static extern void SetItem(string key, string value);

        [DllImport("__Internal")]
        private static extern void RemoveItem(string key);
#endif

        internal IEnumerator StartAuth(string lang, bool signup = false)
        {
            isSignup = signup;
            currentLang = lang;

            ProgressBar.Show(isSignup
                ? Strings.Get(Strings.SigningUp, currentLang)
                : Strings.Get(Strings.LoggingIn, currentLang));

            Uri appUri = new Uri(Application.absoluteURL);
            ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            string accessKey = apiPrefs.accessKey;

            Debug.Log("App url: " + Application.absoluteURL);
            Debug.Log("whitelabelId: " + apiPrefs.whitelabelId);

            Result<string> jwtTokenResult = Result<string>.Undefined();
            Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();

            yield return BaseRepository.Instance.LoadJwtToken(accessKey, (result) => { jwtTokenResult = result; });
            if (!(jwtTokenResult is Result<string>.SuccessResult))
            {
                ProgressBar.Hide();
                ErogamesAuthBridge.Instance.OnAuthError(jwtTokenResult.ErrorMsg);
                yield break;
            }

            yield return BaseRepository.Instance.LoadWhitelabel(jwtTokenResult.Data, apiPrefs.whitelabelId, (result) => { whitelabelResult = result; });
            if (!(whitelabelResult is Result<Whitelabel>.SuccessResult))
            {
                ProgressBar.Hide();
                ErogamesAuthBridge.Instance.OnAuthError(whitelabelResult.ErrorMsg);
                yield break;
            }

            string codeVerifier = Pkce.Instance.GetCodeVerifier();
            string codeChallenge = Pkce.Instance.GenerateCodeChallenge(codeVerifier);
#if UNITY_WEBGL
            SetItem("com_erogames_auth_codeverifier", codeVerifier);
#endif

            string authorizeUrl = whitelabelResult.Data.authorize_url;
            string redirectUri = AuthUtil.BuildRedirectUri(true);
            string clientId = BaseRepository.Instance.GetClientId();
            string authUrl = AuthUtil.BuildAuthUrl(authorizeUrl, codeChallenge, redirectUri, clientId, lang, signup);

#if UNITY_WEBGL
            TakeCodeFromIframe(authUrl);
#endif
        }

        internal static void ReplaceHistoryState(string url)
        {
#if UNITY_WEBGL
            ReplaceHistoryStateInternal(url);
#endif
        }

#if UNITY_WEBGL
        internal static string GetWindowName()
        {
            return GetWindowNameInternal();
        }
#endif

        internal IEnumerator ProceedAuth(string code)
        {
            ProgressBar.Show(isSignup
                ? Strings.Get(Strings.SigningUp, currentLang)
                : Strings.Get(Strings.LoggingIn, currentLang));

            string redirectUri = AuthUtil.BuildRedirectUri();

            Result<Token> tokenResult = Result<Token>.Undefined();
            Result<User> userResult = Result<User>.Undefined();

            string codeVerifier = "";
#if UNITY_WEBGL
            codeVerifier = GetItem("com_erogames_auth_codeverifier");
            RemoveItem("com_erogames_auth_codeverifier");
#endif

            yield return BaseRepository.Instance.LoadToken(code, redirectUri, codeVerifier, (result) => { tokenResult = result; });
            if (!(tokenResult is Result<Token>.SuccessResult))
            {
                ErogamesAuthBridge.Instance.OnAuthError(tokenResult.ErrorMsg);
                ProgressBar.Hide();
                yield break;
            }

            yield return BaseRepository.Instance.LoadUser((result) => { userResult = result; });
            if (!(userResult is Result<User>.SuccessResult))
            {
                ErogamesAuthBridge.Instance.OnAuthError(userResult.ErrorMsg);
                ProgressBar.Hide();
                yield break;
            }

            ProgressBar.Hide();
            ErogamesAuthBridge.Instance.OnAuthSuccess();
        }

        internal IEnumerator OnAuhCode(string codeObjJson)
        {
            Code code = JsonUtility.FromJson<Code>(codeObjJson);
            if (!string.IsNullOrEmpty(code.value))
            {
                yield return ProceedAuth(code.value);
            }
            else
            {
                Application.OpenURL(code.origin);
                yield break;
            }
        }

        internal void Logout(bool webLogout)
        {
            Whitelabel wl = BaseRepository.Instance.GetWhitelabel();
            Token token = BaseRepository.Instance.GetToken();
            string redirectUri = AuthUtil.BuildRedirectUri(true);
            string logoutUrl = AuthUtil.BuildWebLogoutUrl(wl.url, token.access_token, redirectUri);

            BaseRepository.Instance.ClearData();
            ErogamesAuthBridge.Instance.OnLogout();
#if UNITY_WEBGL
            if (webLogout) WebLogoutInternal(logoutUrl);
#endif
        }
    }
}
