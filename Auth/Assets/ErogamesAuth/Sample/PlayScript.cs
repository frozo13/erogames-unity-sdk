using System;
using System.Collections.Generic;
using ErogamesAuthNS.Model;
using UnityEngine;
using UnityEngine.UI;

namespace ErogamesAuthNS.Sample
{
    public class PlayScript : MonoBehaviour
    {
        public GameObject mainCamera;
        public Text userText;
        public Button signupButton;
        public Button loginButton;
        public Button logoutButton;
        public Button testButton;
        public Button reloadUserButton;
        public Button refreshTokenButton;

        private User user;
        private int counter;

        private void Awake()
        {
            ErogamesAuth.OnAuth((User user, string error) =>
            {
                Debug.Log("_OnAuth_");
                if (error !=null) Debug.LogError("OnAuth error: " + error);
                UpdateUI();
            });
        }

        public void OnLogin()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.WebGLPlayer:
                    ErogamesAuth.Login("en");
                    break;
                default:
                    LoginByPassword();
                    break;
            }
        }

        public void OnSignup()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.WebGLPlayer:
                    ErogamesAuth.Signup("en");
                    break;
                default:
                    RegisterUser();
                    break;
            }
        }

        public void OnLogout()
        {
            ErogamesAuth.Logout(true);
        }

        public void OnTest()
        {
            counter++;
            switch (counter)
            {
                case 1:
                    ProceedPayment();
                    break;          
                case 2:
                    LoadWhitelabel();
                    break;
                case 3:
                    LoadQuestData();
                    break;
            }
            if (counter >= 3) counter = 0;
        }

        public void OnReloadUser()
        {
            ErogamesAuth.ReloadUser(() =>
            {
                UpdateUI();
            }, (string error) => Debug.Log("ReloadUser error: " + error));
        }

        public void OnRefreshToken()
        {
            ErogamesAuth.RefreshToken(() =>
            {
                UpdateUI();
            }, (string error) => Debug.Log("RefreshToken error: " + error));
        }

        private void UpdateUI()
        {
            user = ErogamesAuth.GetUser();
            Token token = ErogamesAuth.GetToken();
            string userStr = JsonUtility.ToJson(user);
            string tokenStr = JsonUtility.ToJson(token);

            Debug.Log("updateUI...");
            Debug.Log("user: " + userStr);
            Debug.Log("token: " + tokenStr);

            userText.text = userStr;
            signupButton.gameObject.SetActive(user == null);
            loginButton.gameObject.SetActive(user == null);
            logoutButton.gameObject.SetActive(user != null);
        }

        private void LoginByPassword()
        {
            string username = "username";
            string password = "password";
            ErogamesAuth.LoginByPassword(username, password);
        }

        private void RegisterUser()
        {
            string username = "username";
            string password = "password";
            string email = "email";
            bool checkTermsOfUse = true;

            ErogamesAuth.RegisterUser(
                "client_secret",
                username,
                password,
                email,
                checkTermsOfUse,
                () => ErogamesAuth.LoginByPassword(email, password),
                (string error) => Debug.Log("RegisterUser error: " + error));
        }

        private void ProceedPayment()
        {
            string paymentId = Guid.NewGuid().ToString();
            int amount = 1;
            ErogamesAuth.ProceedPayment(paymentId, amount,
                () => LoadPaymentInfo(paymentId),
                (string error) => Debug.Log("OnProceedPayment error: " + error));
        }

        private void LoadPaymentInfo(string paymentId)
        {
            ErogamesAuth.LoadPaymentInfo(paymentId,
                (PaymentInfo data) => Debug.Log("LoadPaymentInfo: " + JsonUtility.ToJson(data)),
                (string error) => Debug.Log("LoadPaymentInfo error: " + error));
        }

        private void LoadWhitelabel()
        {
            ErogamesAuth.LoadWhitelabel(
                () =>
                {
                    Debug.Log("OnLoadWhitelabel: " + ErogamesAuth.GetWhitelabel().slug);
                },
                (string error) =>
                {
                    Debug.Log("OnLoadWhitelabel error: " + error);
                });
        }

        private void LoadQuestData()
        {
            ErogamesAuth.LoadCurrentQuest(
                (QuestData data) =>
                {
                    Debug.Log("On LoadQuestData: " + JsonUtility.ToJson(data));
                },
                (string error) =>
                {
                    Debug.Log("On LoadQuestData error: " + error);
                });
        }
    }
}
