using UnityEngine;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Repository;
using ErogamesAuthNS.Util;
using System;
using System.Collections.Generic;
using System.Collections;
using ErogamesAuthNS.Api;

namespace ErogamesAuthNS
{
    public delegate void OnAuthCallback(User user, string error);
    public delegate void OnResult<T>(T data);
    public delegate void OnResultSuccessDelegate();
    public delegate void OnResultFailureDelegate(string error);

    internal sealed class ErogamesAuthBridge : Singleton<ErogamesAuthBridge>
    {
        private const string BridgeClassName = "com.erogames.auth.unitywrapper.ErogamesAuthBridge";
        private AndroidJavaClass bridgeJavaClass;

        private event OnAuthCallback OnAuthCallback;
        private User OnAuthSuccessPending;
        private string OnAuthErrorPending;

        private event OnResultSuccessDelegate OnReloadUserSuccessEvent;
        private event OnResultFailureDelegate OnReloadUserFailureEvent;

        private event OnResultSuccessDelegate OnRegisterUserSuccessEvent;
        private event OnResultFailureDelegate OnRegisterUserFailureEvent;

        private event OnResultSuccessDelegate OnRefreshTokenSuccessEvent;
        private event OnResultFailureDelegate OnRefreshTokenFailureEvent;

        private event OnResultSuccessDelegate OnProceedPaymentSuccessEvent;
        private event OnResultFailureDelegate OnProceedPaymentFailureEvent;

        private event OnResultSuccessDelegate OnAddDataPointsSuccessEvent;
        private event OnResultFailureDelegate OnAddDataPointsFailureEvent;

        private event OnResultSuccessDelegate OnLoadWhitelabelSuccessEvent;
        private event OnResultFailureDelegate OnLoadWhitelabelFailureEvent;

        private event OnResult<QuestData> OnLoadCurrentQuestSuccessEvent;
        private event OnResultFailureDelegate OnLoadCurrentQuestFailureEvent;

        private event OnResult<PaymentInfo> OnLoadPaymentInfoSuccessEvent;
        private event OnResultFailureDelegate OnLoadPaymentInfoFailureEvent;

        internal void SetAuthCallback(OnAuthCallback callback)
        {
            OnAuthCallback = callback;
            if (OnAuthSuccessPending != null)
            {
                OnAuthCallback.Invoke(OnAuthSuccessPending, null);
                OnAuthSuccessPending = null;
                OnAuthErrorPending = null;
            }
            if (OnAuthErrorPending != null)
            {
                OnAuthCallback.Invoke(null, OnAuthErrorPending);
                OnAuthSuccessPending = null;
                OnAuthErrorPending = null;
            }
        }

        internal void OnAuthSuccess()
        {
            PrefUtil.IsOnLogin(false);
            OnAuthSuccessPending = GetUser();
            OnAuthErrorPending = null;
            if (OnAuthCallback != null) OnAuthCallback.Invoke(OnAuthSuccessPending, null);
        }

        internal void OnAuthError(string error)
        {
            PrefUtil.IsOnLogin(false);
            OnAuthSuccessPending = null;
            OnAuthErrorPending = error;
            if (OnAuthCallback != null) OnAuthCallback.Invoke(null, OnAuthErrorPending);
        }

        internal void OnLogout()
        {
            if (OnAuthCallback != null) OnAuthCallback.Invoke(null, null);
        }

        internal void OnReloadUserSuccess()
        {
            if (OnReloadUserSuccessEvent != null) OnReloadUserSuccessEvent.Invoke();
        }

        internal void OnReloadUserFailure(string error)
        {
            if (OnReloadUserFailureEvent != null) OnReloadUserFailureEvent.Invoke(error);
        }

        internal void OnRegisterUserSuccess()
        {
            if (OnRegisterUserSuccessEvent != null) OnRegisterUserSuccessEvent.Invoke();
        }

        internal void OnRegisterUserFailure(string error)
        {
            if (OnRegisterUserFailureEvent != null) OnRegisterUserFailureEvent.Invoke(error);
        }

        internal void OnRefreshTokenSuccess()
        {
            if (OnRefreshTokenSuccessEvent != null) OnRefreshTokenSuccessEvent.Invoke();
        }

        internal void OnRefreshTokenFailure(string error)
        {
            if (OnRefreshTokenFailureEvent != null) OnRefreshTokenFailureEvent.Invoke(error);
        }

        internal void OnProceedPaymentSuccess()
        {
            if (OnProceedPaymentSuccessEvent != null) OnProceedPaymentSuccessEvent.Invoke();
        }

        internal void OnProceedPaymentFailure(string error)
        {
            if (OnProceedPaymentFailureEvent != null) OnProceedPaymentFailureEvent.Invoke(error);
        }

        internal void OnAddDataPointsSuccess()
        {
            if (OnAddDataPointsSuccessEvent != null) OnAddDataPointsSuccessEvent.Invoke();
        }

        internal void OnAddDataPointsFailure(string error)
        {
            if (OnAddDataPointsFailureEvent != null) OnAddDataPointsFailureEvent.Invoke(error);
        }

        internal void OnLoadWhitelabelSuccess()
        {
            if (OnLoadWhitelabelSuccessEvent != null) OnLoadWhitelabelSuccessEvent.Invoke();
        }

        internal void OnLoadWhitelabelFailure(string error)
        {
            if (OnLoadWhitelabelFailureEvent != null) OnLoadWhitelabelFailureEvent.Invoke(error);
        }

        internal void OnLoadCurrentQuestSuccess(string questDataJson)
        {
            if (OnLoadCurrentQuestSuccessEvent != null)
            {
                QuestData questData = JsonUtility.FromJson<QuestData>(questDataJson);
                OnLoadCurrentQuestSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadCurrentQuestSuccess(QuestData questData)
        {
            if (OnLoadCurrentQuestSuccessEvent != null)
            {
                OnLoadCurrentQuestSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadCurrentQuestFailure(string error)
        {
            if (OnLoadCurrentQuestFailureEvent != null) OnLoadCurrentQuestFailureEvent.Invoke(error);
        }

        internal void OnLoadPaymentInfoSuccess(string paymentInfoJson)
        {
            if (OnLoadPaymentInfoSuccessEvent != null)
            {
                PaymentInfo questData = JsonUtility.FromJson<PaymentInfo>(paymentInfoJson);
                OnLoadPaymentInfoSuccessEvent.Invoke(questData);
            }
        }

        internal void OnLoadPaymentInfoSuccess(PaymentInfo paymentInfo)
        {
            if (OnLoadPaymentInfoSuccessEvent != null)
            {
                OnLoadPaymentInfoSuccessEvent.Invoke(paymentInfo);
            }
        }

        internal void OnLoadPaymentInfoFailure(string error)
        {
            if (OnLoadPaymentInfoFailureEvent != null) OnLoadPaymentInfoFailureEvent.Invoke(error);
        }

        private AndroidJavaClass GetBridgeJavaClass()
        {
            if (bridgeJavaClass == null)
                bridgeJavaClass = new AndroidJavaClass(BridgeClassName);
            return bridgeJavaClass;
        }

        internal IEnumerator Login(string language)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
                    GetBridgeJavaClass().CallStatic("init", apiPrefs.clientId);
                    GetBridgeJavaClass().CallStatic("login", language);
                    break;
                case RuntimePlatform.WebGLPlayer:
                    yield return WebGLBridge.Instance.StartAuth(language);
                    break;
                default:
                    throw new Exception("Not supported for " + Application.platform);
            }
        }

        internal IEnumerator LoginByPassword(string username, string password)
        {
            ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<string> jwtTokenResult = Result<string>.Undefined();
                Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
                Result<Token> tokenResult = Result<Token>.Undefined();
                Result<User> userResult = Result<User>.Undefined();

                yield return BaseRepository.Instance.LoadJwtToken(apiPrefs.accessKey, (result) => { jwtTokenResult = result; });
                if (!(jwtTokenResult is Result<string>.SuccessResult))
                {
                    OnAuthError(jwtTokenResult.ErrorMsg);
                    yield break;
                }

                yield return BaseRepository.Instance.LoadWhitelabel(jwtTokenResult.Data, apiPrefs.whitelabelId, (result) => { whitelabelResult = result; });
                if (!(whitelabelResult is Result<Whitelabel>.SuccessResult))
                {
                    OnAuthError(whitelabelResult.ErrorMsg);
                    yield break;
                }

                string clientId = ApiPrefsUtil.LoadMergedApiPrefs().clientId;
                yield return BaseRepository.Instance.LoadTokenByPassword(whitelabelResult.Data.token_url, clientId, username, password, (result) => { tokenResult = result; });
                if (!(tokenResult is Result<Token>.SuccessResult))
                {
                    OnAuthError(tokenResult.ErrorMsg);
                    yield break;
                }

                yield return BaseRepository.Instance.LoadUser((result) => { userResult = result; });
                if (!(userResult is Result<User>.SuccessResult))
                {
                    OnAuthError(userResult.ErrorMsg);
                    yield break;
                }

                OnAuthSuccess();
            }
            else
            {
                //GetBridgeJavaClass().CallStatic("loginByPassword", username, password);
                throw new Exception("LoginByPassword: not supported on " + Application.platform.ToString());
            }
        }

        internal IEnumerator Signup(string language)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
                    GetBridgeJavaClass().CallStatic("init", apiPrefs.clientId);
                    GetBridgeJavaClass().CallStatic("signup", language);
                    break;
                case RuntimePlatform.WebGLPlayer:
                    yield return WebGLBridge.Instance.StartAuth(language, true);
                    break;
                default:
                    throw new Exception("Not supported for " + Application.platform);
            }
        }

        internal IEnumerator RegisterUser(string username, string password, string email, bool checkTermsOfUse, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            yield return RegisterUser(null, username, password, email, checkTermsOfUse, onSuccess, onFailure);
        }

        internal IEnumerator RegisterUser(string clientSecret, string username, string password, string email, bool checkTermsOfUse, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnRegisterUserSuccessEvent = onSuccess;
            OnRegisterUserFailureEvent = onFailure;

            ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<string> jwtTokenResult = Result<string>.Undefined();
                Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
                Result<bool> registerUserResult = Result<bool>.Undefined();

                yield return BaseRepository.Instance.LoadJwtToken(apiPrefs.accessKey, (result) => { jwtTokenResult = result; });
                if (!(jwtTokenResult is Result<string>.SuccessResult))
                {
                    OnRegisterUserFailure(jwtTokenResult.ErrorMsg);
                    yield break;
                }

                yield return BaseRepository.Instance.LoadWhitelabel(jwtTokenResult.Data, apiPrefs.whitelabelId, (result) => { whitelabelResult = result; });
                if (!(whitelabelResult is Result<Whitelabel>.SuccessResult))
                {
                    OnRegisterUserFailure(whitelabelResult.ErrorMsg);
                    yield break;
                }

                string baseUrl = whitelabelResult.Data.url;
                baseUrl += baseUrl.EndsWith("/") ? "" : "/";
                string regUserUrl = baseUrl + ApiService.RegisterUserEndPoint;

                yield return BaseRepository.Instance.RegisterUser(regUserUrl, clientSecret, username, password, email, checkTermsOfUse, (result) => { registerUserResult = result; });
                if (!(registerUserResult is Result<bool>.SuccessResult))
                {
                    OnRegisterUserFailure(registerUserResult.ErrorMsg);
                    yield break;
                }

                OnRegisterUserSuccess();
            }
            else
            {
                //GetBridgeJavaClass().CallStatic("registerUser", clientSecret, username, password, email, checkTermsOfUse);
                Debug.LogError("RegisterUser: not supported on " + Application.platform.ToString());
            }
        }

        internal void Logout(bool webLogout)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    GetBridgeJavaClass().CallStatic("logout", webLogout);
                    break;
                case RuntimePlatform.WebGLPlayer:
                    WebGLBridge.Instance.Logout(webLogout);
                    break;
                default:
                    BaseRepository.Instance.ClearData();
                    OnLogout();
                    break;
            }
        }

        internal User GetUser()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string userStr = GetBridgeJavaClass().CallStatic<string>("getUser");
                return JsonUtility.FromJson<User>(userStr);
            }
            return BaseRepository.Instance.GetUser();
        }

        internal IEnumerator ReloadUser(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnReloadUserSuccessEvent = onSuccess;
            OnReloadUserFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<User> reloadUserResult = Result<User>.Undefined();
                yield return BaseRepository.Instance.LoadUser((result) => { reloadUserResult = result; });
                if (!(reloadUserResult is Result<User>.SuccessResult))
                {
                    OnReloadUserFailure(reloadUserResult.ErrorMsg);
                    yield break;
                }
                OnReloadUserSuccess();
            }
            else
            {
                GetBridgeJavaClass().CallStatic("reloadUser");
            }
        }

        internal Token GetToken()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string tokenStr = GetBridgeJavaClass().CallStatic<string>("getToken");
                return JsonUtility.FromJson<Token>(tokenStr);
            }
            return BaseRepository.Instance.GetToken();
        }

        internal IEnumerator RefreshToken(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnRefreshTokenSuccessEvent = onSuccess;
            OnRefreshTokenFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<Token> refreshTokenResult = Result<Token>.Undefined();
                yield return BaseRepository.Instance.RefreshToken((result) => { refreshTokenResult = result; });
                if (!(refreshTokenResult is Result<Token>.SuccessResult))
                {
                    OnRefreshTokenFailure(refreshTokenResult.ErrorMsg);
                    yield break;
                }
                OnRefreshTokenSuccess();
            }
            else
            {
                GetBridgeJavaClass().CallStatic("refreshToken");
            }
        }

        internal IEnumerator ProceedPayment(string paymentId, int amount, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnProceedPaymentSuccessEvent = onSuccess;
            OnProceedPaymentFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<BaseStatusResp> proceedPaymentResult = Result<BaseStatusResp>.Undefined();
                yield return BaseRepository.Instance.ProceedPayment(paymentId, amount, (r) => { proceedPaymentResult = r; });
                if (!(proceedPaymentResult is Result<BaseStatusResp>.SuccessResult))
                {
                    OnProceedPaymentFailure(proceedPaymentResult.ErrorMsg);
                    yield break;
                }
                OnProceedPaymentSuccess();
            }
            else
            {
                GetBridgeJavaClass().CallStatic("proceedPayment", paymentId, amount);
            }
        }

        internal IEnumerator AddDataPoints(List<DataPoint> dataPoints, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnAddDataPointsSuccessEvent = onSuccess;
            OnAddDataPointsFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<BaseStatusResp> addDataPointsResult = Result<BaseStatusResp>.Undefined();
                yield return BaseRepository.Instance.AddDataPoints(dataPoints, (r) => { addDataPointsResult = r; });
                if (!(addDataPointsResult is Result<BaseStatusResp>.SuccessResult))
                {
                    OnAddDataPointsFailure(addDataPointsResult.ErrorMsg);
                    yield break;
                }
                OnAddDataPointsSuccess();
            }
            else
            {
                var dataPointModelStr = JsonUtility.ToJson(new DataPointModel(dataPoints));
                JSONNode jsonNode = JSON.Parse(dataPointModelStr);
                JSONArray dataPointsJSONArray = jsonNode["data_points"].AsArray;
                GetBridgeJavaClass().CallStatic("addDataPoints", dataPointsJSONArray.ToString());
            }
        }

        internal Whitelabel GetWhitelabel()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string whitelabelStr = GetBridgeJavaClass().CallStatic<string>("getWhitelabel");
                return WhitelabelsResp.ToWhitelabel(whitelabelStr);
            }
            return BaseRepository.Instance.GetWhitelabel();
        }

        internal IEnumerator LoadWhitelabel(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            OnLoadWhitelabelSuccessEvent = onSuccess;
            OnLoadWhitelabelFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<string> jwtTokenResult = Result<string>.Undefined();
                Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
                ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();

                yield return BaseRepository.Instance.LoadJwtToken(apiPrefs.accessKey, (r) => { jwtTokenResult = r; });
                yield return BaseRepository.Instance.LoadWhitelabel(jwtTokenResult.Data, apiPrefs.whitelabelId, (r) => { whitelabelResult = r; });
                if (!(whitelabelResult is Result<Whitelabel>.SuccessResult))
                {
                    OnLoadWhitelabelFailure(whitelabelResult.ErrorMsg);
                    yield break;
                }
                OnLoadWhitelabelSuccess();
            }
            else
            {
                GetBridgeJavaClass().CallStatic("loadWhitelabel");
            }
        }

        internal IEnumerator LoadCurrentQuest(OnResult<QuestData> onSuccess, OnResultFailureDelegate onFailure)
        {
            OnLoadCurrentQuestSuccessEvent = onSuccess;
            OnLoadCurrentQuestFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<QuestData> result = Result<QuestData>.Undefined();

                yield return BaseRepository.Instance.GetCurrentQuestData((r) => { result = r; });
                if (!(result is Result<QuestData>.SuccessResult))
                {
                    OnLoadCurrentQuestFailure(result.ErrorMsg);
                    yield break;
                }
                OnLoadCurrentQuestSuccess(result.Data);
            }
            else
            {
                GetBridgeJavaClass().CallStatic("loadCurrentQuest");
            }
        }

        internal IEnumerator LoadPaymentInfo(string paymentId, OnResult<PaymentInfo> onSuccess, OnResultFailureDelegate onFailure)
        {
            OnLoadPaymentInfoSuccessEvent = onSuccess;
            OnLoadPaymentInfoFailureEvent = onFailure;
            if (Application.platform != RuntimePlatform.Android)
            {
                Result<PaymentInfo> result = Result<PaymentInfo>.Undefined();

                yield return BaseRepository.Instance.GetPaymentInfo(paymentId, (r) => { result = r; });
                if (!(result is Result<PaymentInfo>.SuccessResult))
                {
                    OnLoadPaymentInfoFailure(result.ErrorMsg);
                    yield break;
                }
                OnLoadPaymentInfoSuccess(result.Data);
            }
            else
            {
                GetBridgeJavaClass().CallStatic("loadPaymentInfo", paymentId);
            }
        }
    }
}
