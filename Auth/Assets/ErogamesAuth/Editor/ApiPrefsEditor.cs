﻿using UnityEngine;
using UnityEditor;
using ErogamesAuthNS.Model;

namespace ErogamesAuthNS.Editor
{
#if UNITY_EDITOR
    internal class ApiPrefsEditor : EditorWindow
    {
        SerializedObject apiDataSerializedObject;
        private bool autoLogin = true;

        private void OnEnable()
        {
            ApiPrefsScriptableObject apiData = ApiPrefsScriptableObject.LoadOrCreate();
            apiDataSerializedObject = new SerializedObject(apiData);
        }

        // Add menu item named "ErogamesAuth" to the Window menu
        [MenuItem("Window/ErogamesAuth")]
        public static void ShowWindow()
        {
            GetWindow(typeof(ApiPrefsEditor));
        }

        // Update is called once per frame
        void OnGUI()
        {
            GUILayout.Label("ErogamesAuth settings", EditorStyles.boldLabel);
            GUILayout.Label("\n");

            autoLogin = apiDataSerializedObject.FindProperty("autoLogin").boolValue;
            apiDataSerializedObject.FindProperty("autoLogin").boolValue = EditorGUILayout.Toggle("Auto Log in", autoLogin);
            EditorGUILayout.PropertyField(
                apiDataSerializedObject.FindProperty("clientId"), new GUIContent("Client ID"), true);
            EditorGUILayout.PropertyField(
                apiDataSerializedObject.FindProperty("accessKey"), new GUIContent("Access Key"), true);
            EditorGUILayout.PropertyField(
                apiDataSerializedObject.FindProperty("whitelabelId"), new GUIContent("Whitelabel ID"), true);

            EditorGUILayout.HelpBox("Settings will be saved once the window close.", MessageType.Info);
        }

        private void OnDestroy()
        {
            apiDataSerializedObject.ApplyModifiedProperties();
            ApiPrefsScriptableObject apiPrefsScriptableObject = (ApiPrefsScriptableObject)apiDataSerializedObject.targetObject;
            ApiPrefsScriptableObject.Save(apiPrefsScriptableObject);
            apiDataSerializedObject = null;
        }
    }
#endif
}
