﻿using ErogamesAuthNS.Model;

namespace ErogamesAuthNS.Repository
{
    internal interface IStorage
    {
        void SaveToken(Token token);

        Token GetToken();

        void RemoveToken();

        void SaveUser(User user);

        User GetUser();

        void RemoveUser();

        void SetWhitelabel(Whitelabel whitelabel);

        Whitelabel GetWhitelabel();

        void RemoveWhitelabel();
    }
}
