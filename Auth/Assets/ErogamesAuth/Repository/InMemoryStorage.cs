﻿using ErogamesAuthNS.Model;

namespace ErogamesAuthNS.Repository
{
    internal class InMemoryStorage : Singleton<InMemoryStorage>, IStorage
    {
        private User user;
        private Token token;
        private Whitelabel whitelabel;

        public void SaveUser(User user)
        {
            this.user = user;
        }

        public User GetUser()
        {
            return user;
        }

        public void RemoveUser()
        {
            user = null;
        }

        public void SaveToken(Token token)
        {
            this.token = token;
        }

        public Token GetToken()
        {
            return token;
        }

        public void RemoveToken()
        {
            token = null;
        }

        public void SetWhitelabel(Whitelabel whitelabel)
        {
            this.whitelabel = whitelabel;
        }

        public Whitelabel GetWhitelabel()
        {
            return whitelabel;
        }

        public void RemoveWhitelabel()
        {
            whitelabel = null;
        }
    }
}
