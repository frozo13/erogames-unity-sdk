﻿using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Repository
{
    internal class PlayerPrefsStorage : Singleton<PlayerPrefsStorage>, IStorage
    {
        private const string StorageKeyUser = "com_erogames_sdk_unity_storage_user";
        private const string StorageKeyToken = "com_erogames_sdk_unity_storage_token";
        private const string StorageKeyWhitelabel = "com_erogames_sdk_unity_storage_whitelabel";

        /// <summary>
        /// Save Token to local storage.
        /// </summary>
        /// <param name="token"></param>
        public void SaveToken(Token token)
        {
            PlayerPrefs.SetString(StorageKeyToken, JsonUtility.ToJson(token));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Get Token from local storage.
        /// </summary>
        /// <returns></returns>
        public Token GetToken()
        {
            string tokenStr = PlayerPrefs.GetString(StorageKeyToken);
            return (tokenStr != null) ? JsonUtility.FromJson<Token>(tokenStr) : null;
        }

        /// <summary>
        /// Save User to local storage.
        /// </summary>
        /// <param name="user"></param>
        public void SaveUser(User user)
        {
            PlayerPrefs.SetString(StorageKeyUser, JsonUtility.ToJson(user));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Get User from local storage.
        /// </summary>
        /// <returns></returns>
        public User GetUser()
        {
            string userStr = PlayerPrefs.GetString(StorageKeyUser);
            return (userStr != null) ? JsonUtility.FromJson<User>(userStr) : null;
        }

        /// <summary>
        /// Remove User from local storage.
        /// </summary>
        public void RemoveUser()
        {
            PlayerPrefs.DeleteKey(StorageKeyUser);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Remove Token from local storage.
        /// </summary>
        public void RemoveToken()
        {
            PlayerPrefs.DeleteKey(StorageKeyToken);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Save Whitelabel to local storage.
        /// </summary>
        /// <param name="whitelabel"></param>
        public void SetWhitelabel(Whitelabel whitelabel)
        {
            PlayerPrefs.SetString(StorageKeyWhitelabel, JsonUtility.ToJson(whitelabel));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Ret Whitelabel from local storage.
        /// </summary>
        /// <returns></returns>
        public Whitelabel GetWhitelabel()
        {
            string whitelabelStr = PlayerPrefs.GetString(StorageKeyWhitelabel);
            return (whitelabelStr != null) ? JsonUtility.FromJson<Whitelabel>(whitelabelStr) : null;
        }

        /// <summary>
        /// Remove Whitelabel from local storage.
        /// </summary>
        public void RemoveWhitelabel()
        {
            PlayerPrefs.DeleteKey(StorageKeyWhitelabel);
            PlayerPrefs.Save();
        }
    }
}
