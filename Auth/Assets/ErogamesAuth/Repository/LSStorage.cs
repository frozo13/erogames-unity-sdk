﻿using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using UnityEngine;

namespace ErogamesAuthNS.Repository
{
#if UNITY_WEBGL
    internal class LSStorage : Singleton<LSStorage>, IStorage
    {

        private const string StorageKeyUser = "com_erogames_sdk_unity_storage_user";
        private const string StorageKeyToken = "com_erogames_sdk_unity_storage_token";
        private const string StorageKeyWhitelabel = "com_erogames_sdk_unity_storage_whitelabel";

        public void SaveToken(Token token)
        {
            LSPrefs.SetItem(StorageKeyToken, JsonUtility.ToJson(token));
        }

        public Token GetToken()
        {
            string tokenStr = LSPrefs.GetItem(StorageKeyToken);
            return (tokenStr != null) ? JsonUtility.FromJson<Token>(tokenStr) : null;
        }

        public void RemoveToken()
        {
            LSPrefs.RemoveItem(StorageKeyToken);
        }

        public void SaveUser(User user)
        {
            LSPrefs.SetItem(StorageKeyUser, JsonUtility.ToJson(user));
        }

        public User GetUser()
        {
            string userStr = LSPrefs.GetItem(StorageKeyUser);
            return (userStr != null) ? JsonUtility.FromJson<User>(userStr) : null;
        }

        public void RemoveUser()
        {
            LSPrefs.RemoveItem(StorageKeyUser);
        }

        public void SetWhitelabel(Whitelabel whitelabel)
        {
            LSPrefs.SetItem(StorageKeyWhitelabel, JsonUtility.ToJson(whitelabel));
        }

        public Whitelabel GetWhitelabel()
        {
            string whitelabelStr = LSPrefs.GetItem(StorageKeyWhitelabel);
            return (whitelabelStr != null) ? JsonUtility.FromJson<Whitelabel>(whitelabelStr) : null;
        }

        public void RemoveWhitelabel()
        {
            LSPrefs.RemoveItem(StorageKeyWhitelabel);
        }
    }
#endif
}
