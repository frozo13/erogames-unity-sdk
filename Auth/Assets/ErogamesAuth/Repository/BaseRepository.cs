﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAuthNS.Api;
using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;

namespace ErogamesAuthNS.Repository
{
    internal class BaseRepository : Singleton<BaseRepository>
    {
        private static IStorage InMemoryStorage
        {
            get
            {
                return Repository.InMemoryStorage.Instance;
            }
        }

        private static IStorage LocalStorage
        {
            get
            {
#if UNITY_WEBGL
                return Repository.LSStorage.Instance;
#else
                return PlayerPrefsStorage.Instance;
#endif
            }
        }

        /// <summary>
        /// Load Whitelabel and save it to local storage.
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <param name="whitelabelId"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadWhitelabel(string jwtToken, string whitelabelId, Action<Result<Whitelabel>> onResult)
        {
            yield return ApiService.Instance.GetWhitelabels(jwtToken, (result) =>
            {
                if (result is Result<List<Whitelabel>>.SuccessResult)
                {
                    foreach (var whitelabel in result.Data)
                    {
                        if (whitelabel.slug == whitelabelId)
                        {
                            SetWhitelabel(whitelabel);
                            onResult.Invoke(Result<Whitelabel>.Success(whitelabel));
                            return;
                        }
                    }
                    onResult.Invoke(Result<Whitelabel>.Error(string.Format("The whitelabel '{0}' not found.", whitelabelId)));
                    return;
                }
                onResult.Invoke(Result<Whitelabel>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Load Whitelabel and save it to local storage.
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <param name="whitelabelUrl"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadWhitelabelByUrl(string jwtToken, string whitelabelUrl, Action<Result<Whitelabel>> onResult)
        {
            yield return ApiService.Instance.GetWhitelabels(jwtToken, (result) =>
            {
                if (result is Result<List<Whitelabel>>.SuccessResult)
                {
                    foreach (var whitelabel in result.Data)
                    {
                        if (whitelabel.url == whitelabelUrl)
                        {
                            SetWhitelabel(whitelabel);
                            onResult.Invoke(Result<Whitelabel>.Success(whitelabel));
                            return;
                        }
                    }
                    onResult.Invoke(Result<Whitelabel>.Error(string.Format("The whitelabel '{0}' not found.", whitelabelUrl)));
                    return;
                }
                onResult.Invoke(Result<Whitelabel>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Save Whitelabel info to local storage.
        /// </summary>
        /// <param name="whitelabel"></param>
        internal void SetWhitelabel(Whitelabel whitelabel)
        {
            LocalStorage.SetWhitelabel(whitelabel);
        }

        /// <summary>
        /// Get Whitelabel from local storage.
        /// </summary>
        /// <returns>Whitelabel</returns>
        internal Whitelabel GetWhitelabel()
        {
            return LocalStorage.GetWhitelabel();
        }

        /// <summary>
        /// Load JWT token.
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            yield return ApiService.Instance.GetJwtToken(accessKey, onResult);
        }

        /// <summary>
        /// Load Token and save it to local storage.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="redirectUri"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadToken(string code, string redirectUri, string codeVerifier, Action<Result<Token>> onResult)
        {
            Whitelabel whitelabel = GetWhitelabel();
            if (whitelabel == null)
            {
                onResult.Invoke(Result<Token>.Error("The whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.GetToken(whitelabel.token_url, GetClientId(), code, redirectUri, codeVerifier, (result) =>
            {
                if (result is Result<Token>.SuccessResult)
                {
                    SaveToken(result.Data);
                    onResult.Invoke(Result<Token>.Success(result.Data));
                    return;
                }
                onResult.Invoke(Result<Token>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Get client id.
        /// </summary>
        /// <returns>string</returns>
        internal string GetClientId()
        {
            return ApiPrefsUtil.LoadMergedApiPrefs().clientId;
        }

        /// <summary>
        /// Load Token and save it to local storage.
        /// </summary>
        /// <param name="tokenUrl"></param>
        /// <param name="clientId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadTokenByPassword(string tokenUrl, string clientId, string username, string password, Action<Result<Token>> onResult)
        {
            yield return ApiService.Instance.GetTokenByPassword(tokenUrl, clientId, username, password, (result) =>
            {
                if (result is Result<Token>.SuccessResult)
                {
                    SaveToken(result.Data);
                    onResult.Invoke(Result<Token>.Success(result.Data));
                    return;
                }
                onResult.Invoke(Result<Token>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Get Token from local storage.
        /// </summary>
        /// <returns>Token</returns>
        internal Token GetToken()
        {
#if UNITY_WEBGL
            return InMemoryStorage.GetToken();
#else
            return LocalStorage.GetToken();
#endif
        }

        /// <summary>
        /// Save Token to local storage.
        /// </summary>
        /// <param name="token"></param>
        internal void SaveToken(Token token)
        {
#if UNITY_WEBGL
            InMemoryStorage.SaveToken(token);
#else
            LocalStorage.SaveToken(token);
#endif
        }


        internal IEnumerator GetOrRefreshToken(Action<Result<Token>> onResult)
        {
            Token token = GetToken();
            if (token != null && token.IsExpired())
            {
                yield return RefreshToken(onResult);
            }
            else
            {
                onResult.Invoke(Result<Token>.Success(token));
                yield break;
            }
        }


        /// <summary>
        /// Load User and save it to local storage.
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadUser(Action<Result<User>> onResult)
        {
            Token token = null;
            yield return GetOrRefreshToken((t) => token = t.Data);
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<User>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.GetUser(whitelabel.profile_url, string.Format("Bearer {0}", token.access_token), (result) =>
            {
                if (result is Result<User>.SuccessResult)
                {
                    SaveUser(result.Data);
                    onResult.Invoke(Result<User>.Success(result.Data));
                    return;
                }
                onResult.Invoke(Result<User>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Save User to local storage.
        /// </summary>
        /// <param name="user"></param>
        internal void SaveUser(User user)
        {
#if UNITY_WEBGL
            InMemoryStorage.SaveUser(user);
#else
            LocalStorage.SaveUser(user);
#endif
        }

        /// <summary>
        /// Get User from local storage.
        /// </summary>
        /// <returns></returns>
        internal User GetUser()
        {
#if UNITY_WEBGL
            return InMemoryStorage.GetUser();
#else
            return LocalStorage.GetUser();
#endif
        }

        /// <summary>
        /// Clear all local authentication data.
        /// </summary>
        internal void ClearData()
        {
            InMemoryStorage.RemoveUser();
            InMemoryStorage.RemoveToken();
            InMemoryStorage.RemoveWhitelabel();

            LocalStorage.RemoveUser();
            LocalStorage.RemoveToken();
            LocalStorage.RemoveWhitelabel();
        }

        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <param name="clientSecret"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="checkTermsOfUse"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator RegisterUser(string registerUrl, string clientSecret, string username, string password, string email, bool checkTermsOfUse, Action<Result<bool>> onResult)
        {
            return ApiService.Instance.RegisterUser(registerUrl, clientSecret, username, password, email, checkTermsOfUse, onResult);
        }

        /// <summary>
        /// Refresh Token.
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator RefreshToken(Action<Result<Token>> onResult)
        {
            Token token = GetToken();
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<Token>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.RefreshToken(whitelabel.token_url, GetClientId(), token.refresh_token, (result) =>
            {
                if (result is Result<Token>.SuccessResult)
                {
                    SaveToken(result.Data);
                    onResult.Invoke(Result<Token>.Success(result.Data));
                    return;
                }
                onResult.Invoke(Result<Token>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Proceed payment.
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="amount"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator ProceedPayment(string paymentId, int amount, Action<Result<BaseStatusResp>> onResult)
        {
            Token token = null;
            yield return GetOrRefreshToken((t) => token = t.Data);
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<BaseStatusResp>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }


            yield return ApiService.Instance.ProceedPayment(
                whitelabel.url + "/api/v1/payments",
                string.Format("Bearer {0}", token.access_token),
                paymentId,
                amount,
                onResult
            );
        }

        /// <summary>
        /// Add data points.
        /// </summary>
        /// <param name="dataPoints"></param>
        /// <returns>Task<BaseStatusResp></returns>
        internal IEnumerator AddDataPoints(List<DataPoint> dataPoints, Action<Result<BaseStatusResp>> onResult)
        {
            Token token = null;
            yield return GetOrRefreshToken((t) => token = t.Data);
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<BaseStatusResp>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.AddDataPoints(
                whitelabel.url + "/api/v1/me/data_point_collection",
                string.Format("Bearer {0}", token.access_token),
                new DataPointModel(dataPoints),
                onResult
            );
        }

        /// <summary>
        /// Get Current Quest Data
        /// </summary>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetCurrentQuestData(Action<Result<QuestData>> onResult)
        {
            Token token = null;
            yield return GetOrRefreshToken((t) => token = t.Data);
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<QuestData>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.GetCurrentQuestData(
                whitelabel.url + "/api/v1/quests/current",
                string.Format("Bearer {0}", token.access_token),
                onResult
            );
        }

        internal IEnumerator GetPaymentInfo(string paymentId, Action<Result<PaymentInfo>> onResult)
        {
            Token token = null;
            yield return GetOrRefreshToken((t) => token = t.Data);
            Whitelabel whitelabel = GetWhitelabel();
            if (token == null || whitelabel == null)
            {
                onResult.Invoke(Result<PaymentInfo>.Error("The token and/or whitelabel can't be null."));
                yield break;
            }

            yield return ApiService.Instance.GetPaymentInfo(
                whitelabel.url + "/api/v1/payments/" + paymentId,
                string.Format("Bearer {0}", token.access_token),
                onResult
            );
        }
    }
}
