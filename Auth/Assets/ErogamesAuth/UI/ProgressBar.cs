﻿using UnityEngine;

namespace ErogamesAuthNS.UI
{
    public class ProgressBar : MonoBehaviour
    {
        private static readonly string ProgressBarName = "ErogamesAuthNS.UI.ProgressBar";

        private Rect loaderRect = new Rect(0, 0, 160 * SizeScale, 100 * SizeScale);
        private Texture2D spinner;
        private static readonly Texture2D BackgroundTexture = Texture2D.whiteTexture;
        private static readonly GUIStyle TextureStyle =
            new GUIStyle { normal = new GUIStyleState { background = BackgroundTexture } };

        private string message;
        private float angle = 1.0f;
        private readonly float degreesPerSec = 120f;
        private static readonly float SizeScale = 2.0f;

        static public void Show(string message)
        {
            GameObject progressBarObject = GameObject.Find(ProgressBarName);
            if (progressBarObject == null)
            {
                progressBarObject = new GameObject(ProgressBarName);
                ProgressBar progressBar = progressBarObject.AddComponent<ProgressBar>();
                progressBar.Init(message);
            }
        }

        static public void Hide()
        {
            Destroy(GameObject.Find(ProgressBarName));
        }

        void Init(string message)
        {
            this.message = message;
        }

        void OnGUI()
        {
            DrawfullScreenBox();
            DrawLoaderBox();
            DrawSpinner();
            DrawText();
        }

        private void DrawfullScreenBox()
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), GUIContent.none);
        }

        private void DrawLoaderBox()
        {
            var oldColor = GUI.color;
            GUI.color = Color.white;
            loaderRect.x = (int)(Screen.width * 0.5f - loaderRect.width * 0.5f);
            loaderRect.y = (int)(Screen.height * 0.5f - loaderRect.height * 0.5f);
            GUI.Box(loaderRect, GUIContent.none, TextureStyle);
            GUI.color = oldColor;
        }

        private void DrawSpinner()
        {
            var spinnerRect = loaderRect;
            spinnerRect.x += 16 * SizeScale;
            spinnerRect.y += 16 * SizeScale;
            spinnerRect.width -= 32 * SizeScale;
            spinnerRect.height = 42 * SizeScale;

            if (spinner == null)  spinner = Resources.Load<Texture2D>("img_spinner");

            var pivot = new Vector2(spinnerRect.x + spinnerRect.width/2, spinnerRect.y + spinnerRect.height / 2);

            Matrix4x4 matrixBackup = GUI.matrix;
            angle += degreesPerSec * Time.deltaTime;
            GUIUtility.RotateAroundPivot(angle, pivot);
            GUI.DrawTexture(spinnerRect, spinner, ScaleMode.ScaleToFit);
            GUI.matrix = matrixBackup;
        }

        private void DrawText()
        {
            var textRect = loaderRect;
            textRect.x += 16 * SizeScale;
            textRect.y += 16 * SizeScale;
            textRect.width -= 32 * SizeScale;
            textRect.height -= 32 * SizeScale;
                
            var oldColor = GUI.color;
            GUI.color = Color.blue;

            var textStyle = new GUIStyle(GUI.skin.label)
            {
                fontSize = (int)(14 * SizeScale),
                fontStyle = FontStyle.Bold
            };
            textStyle.normal.textColor = new Color(0, 0, 0, 0.54f);
            textStyle.alignment = TextAnchor.LowerCenter;

            GUI.Label(textRect, message, textStyle);

            GUI.color = oldColor;
        }
    }
}
