﻿using System.Runtime.InteropServices;

namespace ErogamesAuthNS.Util
{
#if UNITY_WEBGL
    internal class LSPrefs
    {
        private LSPrefs()
        {
        }

        [DllImport("__Internal")]
        internal static extern void SetItem(string key, string value);

        [DllImport("__Internal")]
        internal static extern string GetItem(string key);

        [DllImport("__Internal")]
        internal static extern void RemoveItem(string key);

        [DllImport("__Internal")]
        internal static extern int HasKey(string key);
    }
#endif
}
