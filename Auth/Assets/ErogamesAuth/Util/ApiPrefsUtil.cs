﻿using System.IO;
using System.Xml.Serialization;
using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class ApiPrefsUtil
    {
        private const string ApiPrefsXmlFile = "ErogamesAuthApiPrefs";

        private ApiPrefsUtil() { }

        internal static ApiPrefs LoadApiPrefs()
        {
            string fileText = Resources.Load<TextAsset>(ApiPrefsXmlFile).text;
            XmlSerializer serializer = new XmlSerializer(typeof(ApiPrefs));
#pragma warning disable IDE0063 // Use simple 'using' statement
            using (StringReader reader = new StringReader(fileText))
#pragma warning restore IDE0063 // Use simple 'using' statement
            {
                return (ApiPrefs)serializer.Deserialize(reader);
            }
        }

        internal static ApiPrefs LoadMergedApiPrefs()
        {
            ApiPrefs apiPrefs = LoadApiPrefs();
            ApiPrefsScriptableObject apiPrefsScriptableObject = ApiPrefsScriptableObject.Load();

            if (apiPrefsScriptableObject != null)
            {
                apiPrefs.whitelabelId = string.IsNullOrEmpty(apiPrefsScriptableObject.whitelabelId)
                    ? apiPrefs.whitelabelId : apiPrefsScriptableObject.whitelabelId;
                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    string whitelabelId = Utils.ParseQueryString(Application.absoluteURL, "whitelabel");
                    if (string.IsNullOrEmpty(whitelabelId)) whitelabelId = "erogames";
                    apiPrefs.whitelabelId = whitelabelId;
                }

                apiPrefs.clientId = string.IsNullOrEmpty(apiPrefsScriptableObject.clientId)
                    ? apiPrefs.clientId : apiPrefsScriptableObject.clientId;
                apiPrefs.autoLogin = apiPrefsScriptableObject.autoLogin;
                apiPrefs.accessKey = string.IsNullOrEmpty(apiPrefsScriptableObject.accessKey)
                    ? apiPrefs.accessKey : apiPrefsScriptableObject.accessKey;
            }
            return apiPrefs;
        }
    }
}