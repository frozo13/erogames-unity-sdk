﻿using System;
using System.Collections.Generic;
using System.Linq;
using ErogamesAuthNS.Model;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAuthNS.Util
{
    public class Utils
    {
        public static StringStringDictionary JSONNodeToDictionary(JSONNode jsonNode)
        {
            StringStringDictionary dict = new StringStringDictionary();
            foreach (string key in jsonNode.Keys)
            {
                dict.Add(key, jsonNode[key]);
            }

            return dict;
        }

        public static string ParseQueryString(string url, string key)
        {
            Uri appUri;
            try
            {
                appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return null;
            }

            string query = appUri.Query;
            query = query.Replace("?", "");
            if (query == null || query.Length < 1 || !query.Contains("=")) return null;
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );

            return keysValues.ContainsKey(key) ? keysValues[key] : null;
        }

        public static string RemoveQueryString(string url, string key)
        {
            if (string.IsNullOrEmpty(url)) return url;
            if (!url.Contains("?")) return url;
            if (!IsUrlValid(url)) return url;

            string left = url.Substring(0, url.IndexOf("?"));
            string query = url.Substring(url.IndexOf("?")).Remove(0, 1);

            if (query == null || query.Length < 1 || !query.Contains("=")) return url;
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );
            keysValues.Remove(key);

            string newQuery = "";
            foreach (string k in keysValues.Keys)
            {
                newQuery += "&" + k + "=" + keysValues[k];
            }

            if (newQuery.Length > 0) newQuery = "?" + newQuery.Remove(0, 1);
            return left + newQuery;
        }

        private static bool IsUrlValid(string url)
        {
            try
            {
                Uri appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return false;
            }
            return true;
        }

        public static bool IsResponseSuccessful(UnityWebRequest unityWebRequest)
        {
            return unityWebRequest.responseCode >= 200 && unityWebRequest.responseCode <= 299;
        }
    }
}
