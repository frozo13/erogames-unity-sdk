﻿using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class Strings
    {
        private static readonly string DefaultLang = "en";

        internal static readonly string LoggingIn = "LoggingIn";
        internal static readonly string SigningUp = "SigningUp";
        internal static readonly string SigningOut = "SigningOut";

        private static readonly Dictionary<string, Dictionary<string, string>> resources = new Dictionary<string, Dictionary<string, string>>()
    {
        {LoggingIn, new Dictionary<string, string>() {
            { DefaultLang, "Logging in..." },
            { "fr", "Connexion..." },
            { "ja", "ログイン中です..." },
            { "zh", "正在登录" },
        }
        },
        {SigningUp, new Dictionary<string, string>() {
            { DefaultLang, "Signing up..." },
            { "fr", "Inscription..." },
            { "ja", "サインアップ中です..." },
            { "zh", "注册" },
        }
        },
        {SigningOut, new Dictionary<string, string>() {
            {DefaultLang, "Signing out..." },
            { "fr", "Déconnexion…" },
            { "ja", "ログアウト中です..." },
            { "zh", "正在登出" },
        }
        },
    };

        private Strings() { }

        internal static string Get(string stringId, string lang)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer) lang = DefaultLang;
            if (!resources.ContainsKey(stringId)) return null;
            if (!resources[stringId].ContainsKey(lang)) return resources[stringId][DefaultLang];
            return resources[stringId][lang];
        }
    }
}