﻿using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class PrefUtil
    {
        private const string PrefKeyOnLogin = "com_erogames_sdk_unity_pref_on_login";

        private PrefUtil()
        {
        }

        internal static bool IsOnLogin()
        {
#if UNITY_WEBGL
            int isOnLoginInt = 0;
            string isOnLoginStr = LSPrefs.GetItem(PrefKeyOnLogin);
            if (int.TryParse(isOnLoginStr, out isOnLoginInt))
            {
                return isOnLoginInt > 0;
            }
            return false;
#else
            return PlayerPrefs.GetInt(PrefKeyOnLogin, 0) > 0;
#endif
        }

        internal static void IsOnLogin(bool onLogin)
        {
#if UNITY_WEBGL
            LSPrefs.SetItem(PrefKeyOnLogin, onLogin ? "1" : "0");
#else
            PlayerPrefs.SetInt(PrefKeyOnLogin, onLogin ? 1 : 0);
            PlayerPrefs.Save();
#endif
        }
    }
}
