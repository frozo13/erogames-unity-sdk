using ErogamesAuthNS.Model;
using ErogamesAuthNS.Util;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAuthNS
{
    public class ErogamesAuth
    {
        private const string DefaultLang = "en";

        private ErogamesAuth() { }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void AutoLogin()
        {
            ApiPrefs apiPrefs = ApiPrefsUtil.LoadMergedApiPrefs();
            if (GetUser() == null)
            {
#if UNITY_ANDROID
                if (apiPrefs.autoLogin && !PrefUtil.IsOnLogin())
                {
                    Login("en");
                }
#endif

#if UNITY_WEBGL
               string windowName =  WebGLBridge.GetWindowName();
               if (windowName == "com_erogames_sdk_auth_iframe") return;

                if (apiPrefs.autoLogin || PrefUtil.IsOnLogin())
                {
                    string code = Utils.ParseQueryString(Application.absoluteURL, "code");
                    if (code != null)
                    {
                        string baseUrl = Utils.RemoveQueryString(Application.absoluteURL, "code");
                        WebGLBridge.ReplaceHistoryState(baseUrl);
                        WebGLBridge.Instance.StartCoroutine(WebGLBridge.Instance.ProceedAuth(code));
                    }
                    else
                    {
                        Login("en");
                    }
                }
#endif
            }
            else
            {
                ErogamesAuthBridge.Instance.OnAuthSuccess();
            }
        }

        /// <summary>
        /// Monitors the authentication state.
        /// </summary>
        /// <param name="onAuthCallback">
        /// will trigger once the user has logged in, signed up, or logged out.
        /// User != null and Error == null: A user is logged in/signed up,
        /// User == null and Error != null: A user is NOT logged in/signed up/logged out,
        /// User == null and Error == null: A user is logged out,
        /// </param>
        public static void OnAuth(OnAuthCallback callback)
        {
            ErogamesAuthBridge.Instance.SetAuthCallback(callback);
        }

        /// <summary>
        /// Starts sign in process. The corresponding "sign in" web URL will be opened in the default browser.
        /// The Custom Chrome Tabs can be used if available.
        /// </summary>
        /// <param name="language">
        /// The preferable language of the "sign in" web page.
        /// </param>
        public static void Login(string language)
        {
            PrefUtil.IsOnLogin(true);
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.Login(language ?? DefaultLang));
        }

        /// <summary>
        /// Log in by username(email)/password. Not available for Android.
        /// Use [OnAuth] to get result.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static void LoginByPassword(string username, string password)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoginByPassword(username, password));
        }

        /// <summary>
        /// Starts sign in process. The corresponding "sign up" web URL will be opened in the default browser.
        /// The Custom Chrome Tabs can be used if available.
        /// </summary>
        /// <param name="language">
        /// The preferable language of the "sign up" web page.
        /// </param>
        public static void Signup(string language)
        {
            PrefUtil.IsOnLogin(true);
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.Signup(language ?? DefaultLang));
        }

        /// <summary>
        /// Register of a new user. Not available for Android.
        /// To "log in" use [login] or [loginByPassword] in [onSuccess].
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="checkTermsOfUse"></param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        [Obsolete("Use RegisterUser(clientSecret, username, password, email, checkTermsOfUse, onSuccess, onFailure)")]
        public static void RegisterUser(
            string username,
            string password,
            string email,
            bool checkTermsOfUse,
            OnResultSuccessDelegate onSuccess,
            OnResultFailureDelegate onFailure)
        {
            RegisterUser("", username, password, email, checkTermsOfUse, onSuccess, onFailure);
        }

        /// <summary>
        /// Register of a new user. Not available for Android.
        /// To "log in" use [login] or [loginByPassword] in [onSuccess].
        /// </summary>
        /// <param name="clientSecret"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="checkTermsOfUse"></param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void RegisterUser(
            string clientSecret,
            string username,
            string password,
            string email,
            bool checkTermsOfUse,
            OnResultSuccessDelegate onSuccess,
            OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.RegisterUser(clientSecret, username, password, email, checkTermsOfUse, onSuccess, onFailure));
        }

        /// <summary>
        /// Clear all local authentication data.
        /// </summary>
        /// <param name="webLogout">
        /// For Android only, if [webLogout] is true then the corresponding "log out" web URL will be opened in the default browser.
        /// In this case, the user will also be logged out from the web site.
        /// </param>
        public static void Logout(bool webLogout = false)
        {
            ErogamesAuthBridge.Instance.Logout(webLogout);
        }

        /// <summary>
        /// Returns the current user data, or null if the user is not logged in.
        /// </summary>
        /// <returns>User</returns>
        public static User GetUser()
        {
            return ErogamesAuthBridge.Instance.GetUser();
        }

        /// <summary>
        /// Refreshes the current user data.
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void ReloadUser(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.ReloadUser(onSuccess, onFailure));
        }

        /// <summary>
        /// Returns the current token, or null if the user is not logged in.
        /// </summary>
        /// <returns>Token</returns>
        public static Token GetToken()
        {
            return ErogamesAuthBridge.Instance.GetToken();
        }

        /// <summary>
        /// Refreshes the current token. For example, can be used if the token is expired.
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void RefreshToken(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.RefreshToken(onSuccess, onFailure));
        }

        /// <summary>
        /// Proceed a new payment.
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="amount">the amount in Erogold</param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void ProceedPayment(string paymentId, int amount, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.ProceedPayment(paymentId, amount, onSuccess, onFailure));
        }

        /// <summary>
        /// Create/update user data points.
        ///
        /// Sending new data points will create a new entries.
        /// Sending a data point that was already created before will update the previous values.
        /// </summary>
        /// <param name="dataPoints"></param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        [Obsolete("Will be removed in v2.0.0")]
        public static void AddDataPoints(List<DataPoint> dataPoints, OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.AddDataPoints(dataPoints, onSuccess, onFailure));
        }

        /// <summary>
        /// Returns a whitelabel info.
        /// </summary>
        /// <returns>Whitelabel</returns>
        public static Whitelabel GetWhitelabel()
        {
            return ErogamesAuthBridge.Instance.GetWhitelabel();
        }

        /// <summary>
        /// Load the whitelabel. For example, can be used if the user is not logged in.
        /// To get already loaded whitelabel <see cref="GetWhitelabel"/>
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadWhitelabel(OnResultSuccessDelegate onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadWhitelabel(onSuccess, onFailure));
        }

        /// <summary>
        /// Loads current quest info.
        ///
        /// Load key quest statistics and fields such as quest title and
        /// description.The data returned also has information about the current ranking of
        /// user's clan (user is loaded based on used auth token) and also current user's
        /// individual contribution to the clan score.This endpoint allows implementing in-game
        /// quest status widgets for more immersive gameplay.
        /// 
        /// </summary>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadCurrentQuest(OnResult<QuestData> onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadCurrentQuest(onSuccess, onFailure));
        }

        /// <summary>
        /// Loads payment information
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="onSuccess"></param>
        /// <param name="onFailure"></param>
        public static void LoadPaymentInfo(string paymentId, OnResult<PaymentInfo> onSuccess, OnResultFailureDelegate onFailure)
        {
            ErogamesAuthBridge.Instance.StartCoroutine(ErogamesAuthBridge.Instance.LoadPaymentInfo(paymentId, onSuccess, onFailure));
        }

        public static bool IsLogging()
        {
            return PrefUtil.IsOnLogin();
        }
    }
}
