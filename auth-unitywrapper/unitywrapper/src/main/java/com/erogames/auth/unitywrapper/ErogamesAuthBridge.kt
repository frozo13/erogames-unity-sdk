package com.erogames.auth.unitywrapper

import androidx.annotation.Keep
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.OnResult
import com.erogames.auth.model.DataPoint
import com.erogames.auth.model.PaymentInfo
import com.erogames.auth.model.QuestData
import com.erogames.auth.unitywrapper.ui.ErogamesAuthBridgeActivity
import com.unity3d.player.UnityPlayer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Keep
object ErogamesAuthBridge {

    private const val TAG = "ErogamesAuthBridge"
    private const val DEFAULT_LANG = "en"

    private const val UNITY_CALLBACK_OBJECT = "ErogamesAuthBridge"
    private const val UNITY_METHOD_ON_SUCCESS = "OnAuthSuccess"
    private const val UNITY_METHOD_ON_FAILURE = "OnAuthError"
    private const val UNITY_METHOD_ON_LOGOUT = "OnLogout"

    private const val UNITY_METHOD_ON_RELOAD_USER_SUCCESS = "OnReloadUserSuccess"
    private const val UNITY_METHOD_ON_RELOAD_USER_FAILURE = "OnReloadUserFailure"

    private const val UNITY_METHOD_ON_REFRESH_TOKEN_SUCCESS = "OnRefreshTokenSuccess"
    private const val UNITY_METHOD_ON_REFRESH_TOKEN_FAILURE = "OnRefreshTokenFailure"

    private const val UNITY_METHOD_ON_REGISTER_USER_SUCCESS = "OnRegisterUserSuccess"
    private const val UNITY_METHOD_ON_REGISTER_USER_FAILURE = "OnRegisterUserFailure"

    private const val UNITY_METHOD_ON_LOAD_WHITELABEL_SUCCESS = "OnLoadWhitelabelSuccess"
    private const val UNITY_METHOD_ON_LOAD_WHITELABEL_FAILURE = "OnLoadWhitelabelFailure"

    private const val UNITY_METHOD_ON_PROCEED_PAYMENT_SUCCESS = "OnProceedPaymentSuccess"
    private const val UNITY_METHOD_ON_PROCEED_PAYMENT_FAILURE = "OnProceedPaymentFailure"

    private const val UNITY_METHOD_ON_ADD_DATA_POINTS_SUCCESS = "OnAddDataPointsSuccess"
    private const val UNITY_METHOD_ON_ADD_DATA_POINTS_FAILURE = "OnAddDataPointsFailure"

    private const val UNITY_METHOD_ON_LOAD_QUEST_SUCCESS = "OnLoadCurrentQuestSuccess"
    private const val UNITY_METHOD_ON_LOAD_QUEST_FAILURE = "OnLoadCurrentQuestFailure"

    private const val UNITY_METHOD_ON_LOAD_PAYMENT_INFO_SUCCESS = "OnLoadPaymentInfoSuccess"
    private const val UNITY_METHOD_ON_LOAD_PAYMENT_INFO_FAILURE = "OnLoadPaymentInfoFailure"

    @JvmStatic
    fun init(clientId: String) {
        UnityPlayer.currentActivity?.let { ErogamesAuth.init(it, clientId) }
    }

    @JvmStatic
    fun signup(locale: String = DEFAULT_LANG) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.signup(it, locale)
        }
    }

    @JvmStatic
    fun registerUser(
        clientSecret: String?,
        username: String?,
        password: String?,
        email: String?,
        checkTermsOfUse: Boolean,
    ) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuth.registerUser(
                it,
                clientSecret ?: "",
                username,
                password,
                email,
                checkTermsOfUse,
                object : OnResult<Void?> {
                    override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                        UNITY_CALLBACK_OBJECT,
                        UNITY_METHOD_ON_REGISTER_USER_SUCCESS,
                        ""
                    )

                    override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                        UNITY_CALLBACK_OBJECT,
                        UNITY_METHOD_ON_REGISTER_USER_FAILURE,
                        ""
                    )
                })
        }
    }

    @JvmStatic
    fun login(locale: String = DEFAULT_LANG) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.login(it, locale)
        }
    }

    @JvmStatic
    fun loginByPassword(username: String?, password: String?) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuth.loginByPassword(it, username, password, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = sendAuthSuccess()
                override fun onFailure(t: Throwable?) =
                    sendAuthFailure(t?.message ?: "unknown error")
            })
        }
    }

    @JvmStatic
    @JvmOverloads
    fun logout(webLogout: Boolean = false) {
        UnityPlayer.currentActivity?.let {
            ErogamesAuthBridgeActivity.logout(it, webLogout)
        }
    }

    @JvmStatic
    fun getUser(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getUser(activity)
                ?.let { user -> return Json { ignoreUnknownKeys = true }.encodeToString(user) }
        }
        return null
    }

    @JvmStatic
    fun reloadUser() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.reloadUser(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_RELOAD_USER_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_RELOAD_USER_FAILURE,
                    t?.message ?: "reloadUser: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun getToken(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getToken(activity)
                ?.let { token -> return Json { ignoreUnknownKeys = true }.encodeToString(token) }
        }
        return null
    }

    @JvmStatic
    fun refreshToken() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.refreshToken(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_REFRESH_TOKEN_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_REFRESH_TOKEN_FAILURE,
                    t?.message ?: "refreshToken: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun getWhitelabel(): String? {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.getWhitelabel(activity)
                ?.let { whitelabel ->
                    return Json { ignoreUnknownKeys = true }.encodeToString(
                        whitelabel
                    )
                }
        }
        return null
    }

    @JvmStatic
    fun loadWhitelabel() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadWhitelabel(activity, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_WHITELABEL_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_WHITELABEL_FAILURE,
                    t?.message ?: "loadWhitelabel: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun proceedPayment(paymentId: String, amount: Int) {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.proceedPayment(activity, paymentId, amount, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_PROCEED_PAYMENT_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_PROCEED_PAYMENT_FAILURE,
                    t?.message ?: "proceedPayment: unknown error"
                )
            })
        }
    }

    @JvmStatic
    @Deprecated("Will be removed.")
    fun addDataPoints(dataPointsStr: String) {
        UnityPlayer.currentActivity?.let { activity ->
            val dataPoints: List<DataPoint>? =
                Json { ignoreUnknownKeys = true }.decodeFromString(dataPointsStr)
            if (dataPoints == null) {
                UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_ADD_DATA_POINTS_FAILURE,
                    "Invalid data: $dataPointsStr"
                )
                return
            }

            ErogamesAuth.addDataPoints(activity, dataPoints, object : OnResult<Void?> {
                override fun onSuccess(data: Void?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_ADD_DATA_POINTS_SUCCESS,
                    ""
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_ADD_DATA_POINTS_FAILURE,
                    t?.message ?: "proceedPayment: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun loadCurrentQuest() {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadCurrentQuest(activity, object : OnResult<QuestData> {
                override fun onSuccess(data: QuestData) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_QUEST_SUCCESS,
                    Json { ignoreUnknownKeys = true }.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_QUEST_FAILURE,
                    t?.message ?: "loadCurrentQuest: unknown error"
                )
            })
        }
    }

    @JvmStatic
    fun loadPaymentInfo(paymentId: String) {
        UnityPlayer.currentActivity?.let { activity ->
            ErogamesAuth.loadPaymentInfo(activity, paymentId, object : OnResult<PaymentInfo> {
                override fun onSuccess(data: PaymentInfo) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_PAYMENT_INFO_SUCCESS,
                    Json { ignoreUnknownKeys = true }.encodeToString(data)
                )

                override fun onFailure(t: Throwable?) = UnityPlayer.UnitySendMessage(
                    UNITY_CALLBACK_OBJECT,
                    UNITY_METHOD_ON_LOAD_PAYMENT_INFO_FAILURE,
                    t?.message ?: "loadPaymentInfo: unknown error"
                )
            })
        }
    }

    fun sendAuthSuccess() {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_SUCCESS,
            ""
        )
    }

    fun sendAuthFailure(error: String) {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_FAILURE,
            error
        )
    }

    fun sendLogout() {
        UnityPlayer.UnitySendMessage(
            UNITY_CALLBACK_OBJECT,
            UNITY_METHOD_ON_LOGOUT,
            ""
        )
    }
}
