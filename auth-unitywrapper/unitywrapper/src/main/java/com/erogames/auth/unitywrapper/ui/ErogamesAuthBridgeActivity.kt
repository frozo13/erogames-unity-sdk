package com.erogames.auth.unitywrapper.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.model.User
import com.erogames.auth.unitywrapper.ErogamesAuthBridge
import java.util.*

internal class ErogamesAuthBridgeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val localeLang = intent.getStringExtra(EXTRA_LOCALE) ?: Locale.ENGLISH.language
        when (intent.action) {
            ACTION_LOGIN -> performLogin(Locale(localeLang))
            ACTION_SIGNUP -> performSignup(Locale(localeLang))
            ACTION_LOGOUT -> performLogout()
        }
    }

    private fun performLogin(locale: Locale) = ErogamesAuth.login(this, locale)

    private fun performSignup(locale: Locale) = ErogamesAuth.signup(this, locale)

    private fun performLogout() {
        val webLogout = intent.getBooleanExtra(EXTRA_WEB_LOGOUT, false)
        ErogamesAuth.logout(this, webLogout)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == ErogamesAuth.REQUEST_CODE_AUTH)
                handleOnAuthActivityResult(data, "Authentication rejected.")
        }

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ErogamesAuth.REQUEST_CODE_AUTH -> handleOnAuthActivityResult(data)
                ErogamesAuth.REQUEST_CODE_LOGOUT -> handleOnLogoutActivityResult()
            }
        }
        finish()
    }

    private fun handleOnAuthActivityResult(data: Intent?, error: String? = null) {
        val user: User? = data?.getParcelableExtra(ErogamesAuth.EXTRA_USER)
        val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR) ?: ""
        when {
            user != null -> ErogamesAuthBridge.sendAuthSuccess()
            else -> ErogamesAuthBridge.sendAuthFailure(error ?: authError)
        }
    }

    private fun handleOnLogoutActivityResult() = ErogamesAuthBridge.sendLogout()

    @Keep
    companion object {
        private const val PREFIX = "com.erogames.auth.unitywrapper"
        private const val ACTION_SIGNUP = "$PREFIX.action.SIGNUP"
        private const val ACTION_LOGIN = "$PREFIX.action.LOGIN"
        private const val ACTION_LOGOUT = "$PREFIX.action.LOGOUT"
        private const val EXTRA_LOCALE = "$PREFIX.extra.LOCALE"
        private const val EXTRA_WEB_LOGOUT = "$PREFIX.extra.WEB_LOGOUT"

        @JvmStatic
        fun signup(ctx: Context, locale: String) {
            val i = Intent(ctx, ErogamesAuthBridgeActivity::class.java)
            i.putExtra(EXTRA_LOCALE, locale)
            i.action = ACTION_SIGNUP
            ctx.startActivity(i)
        }

        @JvmStatic
        fun login(ctx: Context, locale: String) {
            val i = Intent(ctx, ErogamesAuthBridgeActivity::class.java)
            i.action = ACTION_LOGIN
            i.putExtra(EXTRA_LOCALE, locale)
            ctx.startActivity(i)
        }

        @JvmStatic
        fun logout(ctx: Context, webLogout: Boolean) {
            val i = Intent(ctx, ErogamesAuthBridgeActivity::class.java)
            i.action = ACTION_LOGOUT
            i.putExtra(EXTRA_WEB_LOGOUT, webLogout)
            ctx.startActivity(i)
        }
    }
}