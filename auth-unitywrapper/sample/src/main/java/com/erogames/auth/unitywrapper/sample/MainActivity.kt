package com.erogames.auth.unitywrapper.sample

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.erogames.auth.unitywrapper.ErogamesAuthBridge
import java.util.*

class MainActivity : AppCompatActivity() {

    private var locale: Locale = Locale.ENGLISH

    private lateinit var loginBtn: Button
    private lateinit var logoutBtn: Button
    private lateinit var signupBtn: Button
    private lateinit var userTextView: TextView
    private lateinit var spinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginBtn = findViewById(R.id.login)
        loginBtn.setOnClickListener { ErogamesAuthBridge.login("en") }

        signupBtn = findViewById(R.id.signup)
        signupBtn.setOnClickListener { ErogamesAuthBridge.signup("en") }

        logoutBtn = findViewById(R.id.logout)
        logoutBtn.setOnClickListener { ErogamesAuthBridge.logout() }

        userTextView = findViewById(R.id.user)
        spinner = findViewById(R.id.languages)

        initLangSpinner()
    }

    private fun initLangSpinner() {
        ArrayAdapter.createFromResource(
            this,
            R.array.lang_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position > 0) {
                        val lang: String = parent?.getItemAtPosition(position) as String
                        locale = Locale(lang)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            }
        }
    }
}