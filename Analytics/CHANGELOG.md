# Version 1.2.4 - Jun 11, 2021
* Bug fixes and stability improvements.

# Version 1.2.3 - Jun 08, 2021
* Added support for standalone platforms.

# Version 1.2.2 - Jun 03, 2021
* Minor update.

# Version 1.2.1 - Jun 03, 2021
* Update README.

# Version 1.2.0 - May 28, 2021
## Removed
* `com.google.external-dependency-manager` dependency.
## Changed
* Event's data structure.
## Fixed
* Minor bugs.

# Version 1.1.3-preview - Apr 08, 2021
## Fixed
* Deprecation warnings (Unity Editor 2020).
