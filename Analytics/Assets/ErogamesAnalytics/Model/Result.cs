﻿namespace ErogamesAnalyticsNS.Model
{
    public abstract class Result<T>
    {
        readonly public T Data;
        readonly public string ErrorMsg;

        public Result(string errorMsg)
        {
            ErrorMsg = errorMsg;
        }

        public Result(T data, string errorMsg)
        {
            Data = data;
            ErrorMsg = errorMsg;
        }

        public static Result<T> Success(T data)
        {
            return new SuccessResult(data);
        }

        public static Result<T> Error(string error)
        {
            return new ErrorResult(error);
        }

        public static Result<T> Undefined()
        {
            return new UndefinedResult();
        }

        public class SuccessResult : Result<T>
        {
            public SuccessResult(T Data) : base(Data, null) { }
        }

        public class ErrorResult : Result<T>
        {
            public ErrorResult(string error) : base(error) { }
        }

        public class UndefinedResult : Result<T>
        {
            public UndefinedResult() : base("Undefined error message") { }
        }
    }
}
