﻿using System;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Util;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class TrackingDataModel
    {
        private const string Format = "{{" +
            "\"type\": \"{0}\", " +
            "\"category\": \"{1}\", " +
            "\"client_id\": \"{2}\", " +
            "\"tracking_uuid\": \"{3}\", " +
            "\"source_slug\": \"{4}\", " +
            "\"details\": {5}}}";

        private readonly string eventId;
        private readonly string category;
        private readonly string clientId;
        private readonly string trackId;
        private readonly string sourceId;
        private readonly Dictionary<string, string> details;

        internal TrackingDataModel(
            string eventId,
            string category,
            string clientId,
            string trackId,
            string sourceId,
            Dictionary<string, string> details)
        {
            this.eventId = eventId;
            this.category = category;
            this.clientId = clientId;
            this.trackId = trackId;
            this.sourceId = sourceId;
            this.details = details;
        }

        internal string ToJson()
        {
            return string.Format(Format,
                eventId,
                category,
                clientId,
                trackId,
                sourceId,
                Utils.DictionaryToJson(details));
        }
    }
}
