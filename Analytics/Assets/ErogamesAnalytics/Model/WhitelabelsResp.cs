﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class WhitelabelsResp
    {
        internal static List<Whitelabel> ToWhitelabels(string json)
        {
            JSONNode jsonObj = JSON.Parse(json);
            JSONArray whitelabelsNode = jsonObj["whitelabels"].AsArray;
            List<Whitelabel> whitelabels = new List<Whitelabel>();

            foreach (JSONNode whitelabelNode in whitelabelsNode)
            {
                Whitelabel whitelabel = JsonUtility.FromJson<Whitelabel>(whitelabelNode.ToString());
                whitelabels.Add(whitelabel);
            }
            return whitelabels;
        }

        internal static Whitelabel ToWhitelabel(string json)
        {
            JSONNode jsonObj = JSON.Parse(json);
            return JsonUtility.FromJson<Whitelabel>(jsonObj.ToString());
        }
    }
}
