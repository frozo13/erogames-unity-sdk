﻿using System;
using ErogamesAnalyticsNS.Util;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class WhitelabelCache
    {
        public Whitelabel whitelabel;
        public int createdAt;

        internal WhitelabelCache(Whitelabel whitelabel, int createdAt)
        {
            this.whitelabel = whitelabel;
            this.createdAt = createdAt;
        }

        internal bool IsValid()
        {
            return (createdAt + (24 * 60 * 60)) > Utils.GetTimestamp();
        }
    }
}
