﻿using System;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class EventResp
    {
        public string status;

        internal bool IsSuccessfull()
        {
            return "success".Equals(status, StringComparison.OrdinalIgnoreCase);
        }
    }
}
