﻿using ErogamesAnalyticsNS.Model;
using UnityEngine;

namespace ErogamesAnalyticsNS.Repository
{
    internal class Storage
    {
        private const string ClientIdKey = "com_erogames_analytics_pref_client_id";
        private const string WhitelabelIdKey = "com_erogames_analytics_pref_whitelabel_id";
        private const string WLCacheKey = "com_erogames_analytics_pref_wl_cache";

        private Storage() { }

        internal static void SetClientId(string clientId)
        {
            PlayerPrefs.SetString(ClientIdKey, clientId);
        }

        internal static string GetClientId()
        {
            return PlayerPrefs.GetString(ClientIdKey);
        }

        internal static void SetWhitelabelId(string whitelabelId)
        {
            PlayerPrefs.SetString(WhitelabelIdKey, whitelabelId);
        }

        internal static string GetWhitelabelId()
        {
            return PlayerPrefs.GetString(WhitelabelIdKey);
        }

        internal static void SetWhitelabelCache(WhitelabelCache wlCache)
        {
            PlayerPrefs.SetString(WLCacheKey, JsonUtility.ToJson(wlCache));
        }

        internal static WhitelabelCache GetWhitelabelCache()
        {
            string wlCacheStr = PlayerPrefs.GetString(WLCacheKey);
            if (string.IsNullOrEmpty(wlCacheStr)) return null;
            return JsonUtility.FromJson<WhitelabelCache>(wlCacheStr);
        }
    }
}
