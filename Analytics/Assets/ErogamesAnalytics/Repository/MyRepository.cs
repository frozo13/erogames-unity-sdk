﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAnalyticsNS;
using ErogamesAnalyticsNS.Api;
using ErogamesAnalyticsNS.Model;
using ErogamesAnalyticsNS.Util;
using UnityEngine;

namespace ErogamesAnalyticsNS.Repository
{
    internal class MyRepository : Singleton<MyRepository>
    {
        internal void SetClientId(string clientId)
        {
            Storage.SetClientId(clientId);
        }

        internal string GetClientId()
        {
            return Storage.GetClientId();
        }

        internal void SetWhitelabelId(string whitelabelId)
        {
            Storage.SetWhitelabelId(whitelabelId);
        }

        internal string GetWhitelabelId()
        {
            string whitelabelId = Storage.GetWhitelabelId();
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                string wlQueryParam = Utils.ParseQueryString(Application.absoluteURL, "whitelabel");
                if (!string.IsNullOrEmpty(wlQueryParam)) whitelabelId = wlQueryParam;
            }
            return whitelabelId;
        }

        internal IEnumerator SendEvent(TrackingDataModel dataModel, Action<Result<bool>> onResult)
        {
            Result<string> jwtTokenResult = Result<string>.Undefined();
            yield return LoadJwtToken(Constants.AccessKey, (result) => { jwtTokenResult = result; });
            if (!(jwtTokenResult is Result<string>.SuccessResult))
            {
                onResult.Invoke(Result<bool>.Error(jwtTokenResult.ErrorMsg));
                yield break;
            }

            Result<Whitelabel> whitelabelResult = Result<Whitelabel>.Undefined();
            yield return LoadWhitelabel(jwtTokenResult.Data, GetWhitelabelId(), (result) => { whitelabelResult = result; });
            if (!(whitelabelResult is Result<Whitelabel>.SuccessResult))
            {
                onResult.Invoke(Result<bool>.Error(whitelabelResult.ErrorMsg));
                yield break;
            }

            string wlTrackingUrl = BuildWLTrackingUrl(whitelabelResult.Data);
            yield return ApiService.Instance.SendEvent(wlTrackingUrl, dataModel, onResult);
        }

        private string BuildWLTrackingUrl(Whitelabel whitelabel)
        {
            if (whitelabel.url.EndsWith("/")) return whitelabel.url + ApiService.TrackEndpoint;
            return whitelabel.url + "/" + ApiService.TrackEndpoint;
        }

        /// <summary>
        /// Load Whitelabel and save it to local storage.
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <param name="whitelabelId"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadWhitelabel(string jwtToken, string whitelabelId, Action<Result<Whitelabel>> onResult, bool useCache = true)
        {
            if (useCache)
            {
                WhitelabelCache wlCache = Storage.GetWhitelabelCache();
                if (wlCache != null && wlCache.whitelabel.slug == whitelabelId && wlCache.IsValid())
                {
                    Debug.Log("The whitelabel is taken from the cache.");
                    onResult.Invoke(Result<Whitelabel>.Success(wlCache.whitelabel));
                    yield break;
                }
            }

            yield return ApiService.Instance.GetWhitelabels(jwtToken, (result) =>
            {
                if (result is Result<List<Whitelabel>>.SuccessResult)
                {
                    foreach (var whitelabel in result.Data)
                    {
                        if (whitelabel.slug == whitelabelId)
                        {
                            Storage.SetWhitelabelCache(new WhitelabelCache(whitelabel, Utils.GetTimestamp()));
                            onResult.Invoke(Result<Whitelabel>.Success(whitelabel));
                            return;
                        }
                    }
                    onResult.Invoke(Result<Whitelabel>.Error(string.Format("The whitelabel '{0}' not found.", whitelabelId)));
                    return;
                }
                onResult.Invoke(Result<Whitelabel>.Error(result.ErrorMsg));
            });
        }

        /// <summary>
        /// Load JWT token.
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator LoadJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            yield return ApiService.Instance.GetJwtToken(accessKey, onResult);
        }
    }
}
