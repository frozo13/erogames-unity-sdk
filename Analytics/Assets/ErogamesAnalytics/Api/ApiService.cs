﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Model;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAnalyticsNS.Api
{
    internal class ApiService : Singleton<ApiService>
    {
        internal const string TrackEndpoint = "api/v1/tracking";
        internal const string AuthenticateEndpoint = "api/v1/authenticate";
        private const string WhitelabelsEndpoint = "api/v1/whitelabels";

        /// <summary>
        /// Send event
        /// </summary>
        /// <param name="url"></param>
        /// <param name="jwtToken"></param>
        /// <param name="dataModel"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator SendEvent(string url, TrackingDataModel dataModel, Action<Result<bool>> onResult)
        {
            Debug.Log("Tracking url: " + url);
            using (UnityWebRequest unityWebRequest = new UnityWebRequest(url, "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(dataModel.ToJson());
                unityWebRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
                unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
                unityWebRequest.SetRequestHeader("Content-Type", "application/json");
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    EventResp eventResp = JsonUtility.FromJson<EventResp>(unityWebRequest.downloadHandler.text);
                    if (eventResp.IsSuccessfull())
                        onResult.Invoke(Result<bool>.Success(true));
                    else
                        onResult.Invoke(Result<bool>.Error(eventResp.status));
                }
                else
                {
                    onResult.Invoke(Result<bool>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        internal IEnumerator GetWhitelabels(string jwtToken, Action<Result<List<Whitelabel>>> onResult)
        {
            string url = Constants.BaseURL + WhitelabelsEndpoint;
            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(url))
            {
                unityWebRequest.SetRequestHeader("Authorization", jwtToken);
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    List<Whitelabel> wls = WhitelabelsResp.ToWhitelabels(unityWebRequest.downloadHandler.text);
                    onResult.Invoke(Result<List<Whitelabel>>.Success(wls));
                }
                else
                {
                    onResult.Invoke(Result<List<Whitelabel>>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Get JWT token
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator GetJwtToken(string accessKey, Action<Result<string>> onResult)
        {
            string url = Constants.BaseURL + AuthenticateEndpoint;
            WWWForm form = new WWWForm();
            form.AddField("access_key", accessKey);

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(url, form))
            {
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2020_1_OR_NEWER
                if (unityWebRequest.result == UnityWebRequest.Result.Success)
#elif UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    var token = JsonUtility.FromJson<JwtTokenResp>(unityWebRequest.downloadHandler.text).token;
                    onResult.Invoke(Result<string>.Success(token));
                }
                else
                {
                    onResult.Invoke(Result<string>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Extract error message from response.
        /// </summary>
        /// <param name="rawResponse"></param>
        /// <returns></returns>
        private string ExtractErrorMessage(string rawResponse)
        {
            ErrorResp errorResp;
            try
            {
                errorResp = JsonUtility.FromJson<ErrorResp>(rawResponse);
            }
            catch (Exception e)
            {
                return e.Message;
            }

            if (errorResp == null) return rawResponse;
            if (!string.IsNullOrEmpty(errorResp.message)) return errorResp.message;
            if (errorResp.errors != null && errorResp.errors.Count > 0) return string.Join(", ", errorResp.errors.ToArray());
            if (errorResp.messages != null && errorResp.messages.Count > 0) return string.Join(", ", errorResp.messages.ToArray());
            if (!string.IsNullOrEmpty(errorResp.error_description)) return errorResp.error_description;

            return rawResponse;
        }
    }
}
