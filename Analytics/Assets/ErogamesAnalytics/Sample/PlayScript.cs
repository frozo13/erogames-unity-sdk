using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ErogamesAnalyticsNS.Sample
{
    public class PlayScript : MonoBehaviour
    {
        public GameObject mainCamera;
        public Button sendEventButton1;
        public Button sendEventButton2;
        public Button sendEventButton3;

        void Awake()
        {
            ErogamesAnalytics.Init("pG8uvkv4nPzsTFWoW_kJnqzdOxa0Us576zastDoROaE");
        }

        public void OnEvent1()
        {
            ErogamesAnalytics.LogEvent("install");
        }

        public void OnEvent2()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["ea_category"] = "banners2";
            ErogamesAnalytics.LogEvent("sign_in", data);
        }

        public void OnEvent3()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["ea_category"] = "banners3";
            data["custom_param"] = "custom_value";
            ErogamesAnalytics.LogEvent("logout", data);
        }
    }
}
