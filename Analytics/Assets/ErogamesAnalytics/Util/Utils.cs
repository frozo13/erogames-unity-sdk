﻿using System;
using System.Collections.Generic;
using System.Linq;
using ErogamesAnalyticsNS.Model;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAnalyticsNS.Util
{
    internal class Utils
    {
        private Utils() { }
        internal static string ParseQueryString(string url, string key)
        {
            return ParseQueryString(url, key, null);
        }

        internal static string ParseQueryString(string url, string key, string defaultValue)
        {
            Dictionary<string, string> queryParams = ParseQueryStrings(url);
            return queryParams.ContainsKey(key) ? queryParams[key] : defaultValue;
        }

        internal static Dictionary<string, string> ParseQueryStrings(string url)
        {
            Uri appUri;
            try
            {
                appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return new Dictionary<string, string>();
            }

            string query = appUri.Query;
            query = query.Replace("?", "");
            if (query == null || query.Length < 1 || !query.Contains("=")) return new Dictionary<string, string>();
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );

            return keysValues;
        }

        internal static StringStringDictionary JSONNodeToDictionary(JSONNode jsonNode)
        {
            StringStringDictionary dict = new StringStringDictionary();
            foreach (string key in jsonNode.Keys)
            {
                dict.Add(key, jsonNode[key]);
            }

            return dict;
        }

        internal static string DictionaryToJson(Dictionary<string, string> dict)
        {
            if (dict == null) return "{}";
            List<string> keyValueList = new List<string>();
            foreach (KeyValuePair<string, string> entry in dict)
            {
                keyValueList.Add(string.Format("\"{0}\": \"{1}\"", entry.Key, entry.Value));
            }
            return "{" + string.Join(",", keyValueList.ToArray()) + "}";
        }

        internal static bool IsResponseSuccessful(UnityWebRequest unityWebRequest)
        {
            return unityWebRequest.responseCode >= 200 && unityWebRequest.responseCode <= 299;
        }

        internal static int GetTimestamp()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}
