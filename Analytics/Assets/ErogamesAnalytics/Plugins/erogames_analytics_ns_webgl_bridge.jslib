mergeInto(LibraryManager.library, {

  SetCookie: function(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },

  GetCookie: function(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  },

    GetOrCreateTrackId__deps: ['SetCookie', 'GetCookie'],
    GetOrCreateTrackId: function(defaultUuid) {
        defaultUuid = Pointer_stringify(defaultUuid);

        var cname = "ea:track_id";
        var trackId = _GetCookie(cname);
        if (trackId == "") {
            trackId = defaultUuid;
            _SetCookie(cname, defaultUuid, 365);
        }

        const bufferSize = lengthBytesUTF8(trackId) + 1;
        const buffer = _malloc(bufferSize);
        stringToUTF8(trackId, buffer, bufferSize);
        return buffer;
    },
});
