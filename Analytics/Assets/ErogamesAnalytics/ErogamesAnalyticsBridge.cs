﻿using System.Collections;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Model;
using ErogamesAnalyticsNS.Util;
using UnityEngine;
using System;
#if UNITY_WEBGL
using System.Runtime.InteropServices;
#endif
using ErogamesAnalyticsNS.Repository;

namespace ErogamesAnalyticsNS
{
    internal sealed class ErogamesAnalyticsBridge : Singleton<ErogamesAnalyticsBridge>
    {

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern string GetOrCreateTrackId(string defaultUuid);
#endif

        internal void Init(string clientId, string whitelabelId)
        {
            MyRepository.Instance.SetClientId(clientId);
            MyRepository.Instance.SetWhitelabelId(whitelabelId);
        }

        private static List<string> _ReservedPrefixes;
        private static List<string> ReservedPrefixes
        {
            get
            {
                if (_ReservedPrefixes == null)
                {
                    _ReservedPrefixes = new List<string>();
                    _ReservedPrefixes.Add("ea_");
                    return _ReservedPrefixes;
                }
                return _ReservedPrefixes;
            }
        }

        internal void LogEvent(string eventId, Dictionary<string, string> data)
        {
            StartCoroutine(CompleteLogEvent(eventId, data));
        }

        private IEnumerator CompleteLogEvent(string eventId, Dictionary<string, string> data)
        {
            Dictionary<string, string> nonReservedParams = ExtractNonReservedParams(data);

            TrackingDataModel dataModel = new TrackingDataModel(
                eventId,
                GetCategory(data),
                GetClientId(),
                GetTrackId(data),
                GetSourceId(data),
                nonReservedParams
            );

            Debug.Log("Send event data: " + dataModel.ToJson());
            yield return MyRepository.Instance.SendEvent(dataModel, (result) =>
            {
                string status = (result is Result<bool>.SuccessResult) ? "SUCCESS" : "FAILURE";
                Debug.Log("Send event status: " + status);
                if (!(result is Result<bool>.SuccessResult))
                    Debug.LogError("Send event error: " + result.ErrorMsg);
            });
        }

        private string GetCategory(Dictionary<string, string> data)
        {
            if (!data.ContainsKey("ea_category")) return "unknown";
            return data["ea_category"];
        }

        private string GetClientId()
        {
            return MyRepository.Instance.GetClientId();
        }

        private string GetTrackId(Dictionary<string, string> data)
        {
            string trackId = SystemInfo.deviceUniqueIdentifier;

            if (Application.platform != RuntimePlatform.WebGLPlayer
                && SystemInfo.unsupportedIdentifier == trackId)
                Debug.LogWarning("SystemInfo.unsupportedIdentifier is used as a trackId!");

#if UNITY_WEBGL
            return GetOrCreateTrackId(Guid.NewGuid().ToString());
#endif
            return trackId;
        }

        private string GetSourceId(Dictionary<string, string> data)
        {
            if (data.ContainsKey("ea_source_id")) return data["ea_source_id"];
            string sourceId = "unknown";
#if UNITY_WEBGL
            return Utils.ParseQueryString(Application.absoluteURL, "ea_source_id", "unknown");
#endif
            return sourceId;
        }

        private Dictionary<string, string> ExtractNonReservedParams(Dictionary<string, string> data)
        {
            Dictionary<string, string> newDictionary = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> entry in data)
            {
                bool isKeyValid = true;
                foreach (string prefix in ReservedPrefixes)
                {
                    if (entry.Key.StartsWith(prefix)) isKeyValid = false;
                }
                if (isKeyValid) newDictionary.Add(entry.Key, entry.Value);
            }
            return newDictionary;
        }
    }
}
